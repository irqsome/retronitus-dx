Retronitus DX  -  Noise from the past
=====================================

![DX Logo](logo/rsndlogo_8by7.png "Noise from the past!")

Retronitus DX is a configurable music synthesizer for the Parallax "Propeller" P8X32A microcontroller.

Features include:
 - 8 channels
 - 64kHz sample rate
 - built-in sequencer
 - flexible instrument programming
 - waveforms selectable at compile time
 - C++ implementation for PC available
 
Some of example tunes can be found here: https://www.youtube.com/playlist?list=PLjK0tF9KYgXF9byX7UaksyQkB43NOC3AY

## Using the native version

Use the "Blaster Shot" tool to generate PASM code for your desired channel configuration.
You can use it from the command line to extract channel config from song binary metadata or as a Ruby library.

## Using the C++ version

Copy `retrosnd.cpp` and `retrosnd.hpp` to a convenient location in your project. In the same directory, create a `retrosnd_env.hpp` and define the types and function it needs. (The `retrosnd_env.hpp` is written to work in conjunction with `retrosnd_dll.cpp` to build a standalone library. You can also just use that if it is easier for you.)

## RSX format

An RSX file consists of three sections:
 1. 512 bytes of header
 2. (512 * 16 * N) bytes of music data
 3. (512 * M) bytes of sound effect data

### RSX header

    000..00F String "-RETROSNDBUNDLE-"
    010      Zero
    011..019 Reserved (emit zeroes)
    01A..01A Count of songs in this RSX
    01B..01B Count of effects in this RSX
    01C..01F Offset to music data (from start of file in units of 512 bytes)
    020..023 Offset to sound effect data (from start of file in units of 512 bytes)
    024..027 Offset to music metadata (from start of file in units of 512 bytes). Zero if no metadata is available.
    024..1FF Reserved (emit zeroes)

### Music data
Each song is 8192 bytes (= 16 * 512) long.

    0000..07BF compiled Cog image
    07C0..07DF Channel configuration, same format as in song metadata
    07E0..07FE Reserved (emit zeroes)
    07FF..07FF 0x01 if song doesn't support SFX, zero otherwise
    0800..1DFF Music data (starting with SEQ_INIT (i.e. no flag word, no metadata))
    1E00..1FFF If song doesn't support SFX, more data, otherwise Don't care

### Effect data
Each effect is 512 bytes long.

    000..001 Effect length (unit implementation dependent, usually 1/59.9th second)
    002..003 Reserved (emit zeroes)
    004..1FF Effect data

### Music metadata
There is one 512 byte metadata block for each song in the bundle. This contains any number of zero-terminated tags with a type and content, in the form `TYPEContent`. Each tag must be padded with extra zeroes to reach a multiple of 4 bytes. Unused space is to be filled with zeroes. Any data after the first zero-where-a-tag-type-would-be-expected (= a terminator long) must be ignored. Tag types must be four characters in size and may only use ASCII alphanumerics. Tag content may use any character that is in the intersection of ISO 8859-15 and the Parallax ROM codepage. Line Feeds are also allowed.

An example (as PASM):
```spin
DAT
org
byte "TITL","My amazing song that everyone thinks is hype AF",0
long
byte "AUTH","Wuerfel_21",0
long
byte "COMM","This is an incredibly insightful comment."
long 0 ' terminator long
long 0[128-$]
```

#### Known tag types


|Tag |Meaning                      |
|:--:|-----------------------------|
|TITL|Song title                   |
|AUTH|Author                       |
|COMM|Comment                      |



## FAQ
No one actually asks me anything about this, these are just so I don't forget, lol

### Q: Why are there these weird noise bursts? WTF?
[A: Load Retronitus DX into a cog that is not too far away from the pins!](https://forums.parallax.com/discussion/comment/1513556/#Comment_1513556)

### Q: When playing from an RSX, my songs glitch out after a while! WTF?
A: Your unused channels must have a valid pattseq pointer, NOT zero. (Just look at the example songs)

### Q: When playing from an RSX, all the notes are weirdly detuned! WTF?
A: Update your Homespun to 0.32p2


