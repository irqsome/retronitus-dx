# encoding: utf-8
# frozen_string_literal: true

module BeepBox2Spin

    Channel = Struct.new(:name,:subchan_id,:datachan_id,:patterns,:melodic,keyword_init: true)
    Instrument = Struct.new(:name,:arpeggiate,:sort_pitches,:second_pitch,:index,keyword_init: true)
    Pattern = Struct.new(:name,:parts,:instrument,keyword_init:true)
    Note    = Struct.new(:delta,:delay,:pitchbend,:volume)
    PercussionPitch = Struct.new(:pitch) do
        def inspect
            "percussion #{pitch.inspect}"
        end
    end
    PatternPart = Struct.new(:startpitch,:notes) do

        def <<(note)
            if note.delta.kind_of?(Integer) && !BeepBox2Spin.valid_delta?(note.delta,note.delay)
                raise "attempted to add invalid delta #{note.delta.inspect}"
            end
            notes << note
        end
        
        def last_pitch(startpitch_override=nil)
            startpitch = startpitch_override || self.startpitch
            return nil if startpitch.nil?
            return notes.reduce(startpitch) do |pitch_now,note|
                delta = note.delta
                if delta.kind_of? Integer
                    pitch_now +delta
                else pitch_now end
            end
        end
        
        def valid_pitch?(pitch,delay)
            case pitch
            when nil,:noteoff,PercussionPitch
                true
            when Integer
                BeepBox2Spin.valid_delta?(pitch-last_pitch,delay)
            else raise
            end
        end

        def optimize
            i = 0
            while i < notes.size
                note = notes[i]
                next_note = notes[i+1]
                if (note.delay == 8 || next_note == nil || next_note.delta != nil || 
                    note.pitchbend != 0 || next_note.pitchbend != 0 ||
                    note.volume != 100 || next_note.volume != 100)
                    i+=1
                else
                    steal_amount = [8-note.delay,next_note.delay].min
                    note.delay += steal_amount
                    if (next_note.delay -= steal_amount) == 0
                        notes.delete_at i+1
                    else
                        i+=1
                    end
                end
            end
        end

        def total_delay
            notes.sum &:delay
        end
    end

    # How many more ticks do we need if arpeggios are used
    ARPEGGIO_TICKMUL = {3=>4, 4=>4, 6=>2, 8=>2, 24=>1}
    # How many ticks is each arp step (2 note arp)
    ARPEGGIO_STEPS_2 = {3=>1, 4=>2, 6=>1, 8=>2, 24=>4}
    # How many ticks is each arp step (3 or more notes)
    ARPEGGIO_STEPS_3 = {3=>1, 4=>1, 6=>1, 8=>1, 24=>2}
    ARPEGGIO_STEPS = {2=>ARPEGGIO_STEPS_2,3=>ARPEGGIO_STEPS_3,4=>ARPEGGIO_STEPS_3}
    ARPEGGIO_PATTERN = {2=>[0,1],3=>[0,1,2,1],4=>[0,1,2,3]}

    KEY2BASEPITCH = {
        "C" => 12, "C#" => 13,
        "D" => 14, "D#" => 15,
        "E" => 16,
        "F" => 17, "F#" => 18,
        "G" => 19, "G#" => 20,
        "A" => 21, "A#" => 22,
        "B" => 23,
    }


def self.empty_pattern(steps,datachan_id)
    Pattern.new(name:"patt_c%02d_empty_%d" % [datachan_id,steps],instrument:nil,parts:[
        PatternPart.new(nil,[Note.new(nil,steps)])
    ])
end

def self.valid_delta?(delta,delay)
    (delay == 1 ? -11..12 : -12..12).include?(delta) ? delta : false
end

# To deal with new beepbox format
def self.get_instrument(datapatt)
    return datapatt[:instrument] if datapatt[:instrument] 
    raise "No instruments array" if datapatt[:instruments].nil?
    raise "More than one instrument?" if datapatt[:instruments].size != 1
    return datapatt[:instruments].first
end

# Convert BeepBox data structure into Spin template
def self.convert(data,tickdiv_in:1)
    data_format         = data[:format]
    data_format_version = data[:version]
    warn "Warning: unknown format #{data_format}" unless data_format == "BeepBox"
    warn "Warning: unknown format version #{data_format_version}" unless data_format_version == 8

    instruments = []

    arpeggios_used = data[:channels].any? do |datachan|
        datachan[:patterns].each_with_index.any? do |datapatt,datapatt_id|
            datachan[:instruments][get_instrument(datapatt)-1][:chord] == "arpeggio" &&
                datachan[:sequence].include?(datapatt_id+1) &&
                (datapatt[:notes].map{|n| n[:pitches].size}.max || 0) > 1
        end
    end
    warn "Info: Arpeggios used? #{arpeggios_used}!"
    basepitch = KEY2BASEPITCH[data[:key].gsub /\u266f/,?#]
    tickmul = Rational(arpeggios_used ? ARPEGGIO_TICKMUL[data[:ticksPerBeat]] : 1,tickdiv_in)
    ticks_per_second = data[:beatsPerMinute] * 60 * data[:ticksPerBeat] * tickmul
    ticks_per_pattern = data[:beatsPerBar] * data[:ticksPerBeat] * tickmul

    channels = data[:channels].flat_map.with_index do |datachan,datachan_id|
        melodic = datachan[:type] == "pitch"
        data_instruments = datachan[:instruments]

        
        chan_instruments = datachan[:instruments].map.with_index do |instr,instr_id|
            interval = instr[:interval] || instr[:unison]
            puts "#{datachan_id} #{instr_id} #{interval}"
            dual_voice = !(interval == nil || interval == "union" || interval == "none")
            second_pitch = unless dual_voice then nil else case interval
                when "fifth"
                    7
                when "octave"
                    12
                else 0
            end end
            next Instrument.new(name: "instr_c%02d_%02d" % [datachan_id,instr_id],
                                arpeggiate:   instr[:chord] == "arpeggio",
                                sort_pitches: instr[:chord] == "harmony",
                                second_pitch:second_pitch)
        end

        instruments.concat chan_instruments

        max_polyphony = datachan[:patterns].map do |datapatt|
            next 0 if datapatt[:notes].size == 0
            instr = chan_instruments[get_instrument(datapatt)-1]
            max_chord =  if instr.arpeggiate then 1
                else datapatt[:notes].map{|n| n[:pitches].size}.max || 0 end
            max_chord *= 2 unless instr.second_pitch.nil?
            next max_chord
        end.max

        subchans = (0...max_polyphony).map{|i|Channel.new(datachan_id:datachan_id,subchan_id:i,patterns:[],melodic:melodic,
                                                        name: "chan%02d_%d" % [datachan_id,i])}

        my_empty_pattern = empty_pattern(ticks_per_pattern,datachan_id)
        chanpatterns = Array.new(datachan[:patterns].size){[my_empty_pattern]}
        datachan[:patterns].each.with_index(1) do |datapatt,pattnum| # Patterns are 1-indexed (since 0 = no pattern)
            begin
                instr = chan_instruments[get_instrument(datapatt)-1]
                second_pitch = instr.second_pitch
                dual_chan = second_pitch == 0

                notes = datapatt[:notes]

                channelparts = Array.new(max_polyphony){[]}
                current_parts = [nil] * max_polyphony
                ticks_now = 0
                active_chans = max_polyphony # TODO figure out initial value properly to avoid spurious note-offs
                notes.each do |note|
                    points = note[:points]
                    rest_ticks =  points.first[:tick]*tickmul - ticks_now
                    if rest_ticks > 0
                        # Generate noteoff for rest
                        (0...active_chans).each do |nochan|
                            push_noteon(nochan,channelparts,current_parts,:noteoff,rest_ticks)
                        end
                        (active_chans...max_polyphony).each do |nochan|
                            push_noteon(nochan,channelparts,current_parts,nil,rest_ticks)
                        end
                        active_chans = 0
                        ticks_now += rest_ticks
                    end

                    note_length = (points.last[:tick]-points.first[:tick])*tickmul
                    pitches = note[:pitches].map{|x|x+basepitch}
                    do_arpeggio = false
                    new_chans = 0

                    pitches.sort! if instr.sort_pitches
                    
                    if pitches.size > 1 && instr.arpeggiate # Arpeggio
                        do_arpeggio = true
                        new_chans = second_pitch ? 2 : 1
                    elsif second_pitch && !dual_chan # Instrument harmony
                        pitches = pitches.flat_map do |pitch|
                            next pitch,pitch+second_pitch
                        end
                        new_chans = pitches.size
                    elsif dual_chan # dual channel detune
                        new_chans = pitches.size * 2
                    else
                        new_chans = pitches.size
                    end
                    # Generate noteoff/void for unused channels
                    (new_chans...max_polyphony).each do |nochan|
                        push_noteon(nochan,channelparts,current_parts,nochan < active_chans ? :noteoff: nil,note_length)
                    end
                    
                    unless do_arpeggio
                        points.each_cons(2).with_index do |(pt,next_pt),pt_i|
                            event_length = (next_pt[:tick]-pt[:tick])*tickmul
                            (0...new_chans).each do |chan_i|
                                if pt_i == 0 # note on
                                    pitch_i = dual_chan ? chan_i/2 : chan_i
                                    pitch = pitches[pitch_i]
                                    pitch = PercussionPitch[pitch] unless melodic
                                    push_noteon(chan_i,channelparts,current_parts,pitch,event_length,pt[:volume],pt[:pitchBend])
                                else # volume/pitchbend/whatever
                                    push_noteon(chan_i,channelparts,current_parts,nil,event_length,pt[:volume],pt[:pitchBend])
                                end
                            end
                        end
                    else # do arpeggio
                        raise unless melodic
                        event_length = note_length # ignore middle points
                        raise unless arpeggio_steps =   ARPEGGIO_STEPS  .dig(pitches.size,data[:ticksPerBeat])
                        raise unless arpeggio_pattern = ARPEGGIO_PATTERN.dig(pitches.size).cycle
                        arpeggio_notes,arpeggio_remainder = event_length.divmod arpeggio_steps
                        if arpeggio_remainder != 0
                            warn "Warning: arpeggio length (#{event_length}) at tick #{points.first[:tick]} of pattern #{pattnum} of channel #{datachan_id} does not divide by #{arpeggio_steps} evenly, manual fixup required"
                        end
                        
                        arpeggio_notes.times do |arp_i|
                            pitch = pitches[arpeggio_pattern.next]
                            push_noteon(0,channelparts,current_parts,pitch,arpeggio_steps)
                            push_noteon(1,channelparts,current_parts,pitch+second_pitch,arpeggio_steps) if second_pitch
                        end
                    end
                    active_chans = new_chans
                    ticks_now += note_length
                end

                ticks_left = ticks_per_pattern - ticks_now
                if ticks_left > 0
                    # Generate noteoff for rest
                    (0...active_chans).each do |nochan|
                        push_noteon(nochan,channelparts,current_parts,:noteoff,ticks_left)
                    end
                    (active_chans...max_polyphony).each do |nochan|
                        push_noteon(nochan,channelparts,current_parts,nil,ticks_left)
                    end
                elsif ticks_left < 0 then raise
                end

                current_parts.each_with_index do |part,partchan|
                    channelparts[partchan] << part if part
                end

                channelparts.each_with_index do |parts,chan_i|
                    chanpatterns[chan_i][pattnum] = parts.empty? ? my_empty_pattern :  Pattern.new(
                        name: max_polyphony == 1 ? 
                            "patt_c%02d_%03d" % [datachan_id,pattnum] :
                            "patt_c%02d_%03d_s%d" % [datachan_id,pattnum,chan_i],
                        parts:parts,instrument: instr)
                end
            rescue
                puts "Exception occured converting pattern #{pattnum} of channel #{datachan_id}"
                raise
            end
        end

        subchans.each_with_index do |subchan,chan_i|
            subchan.patterns = datachan[:sequence].map do |patt_i|
                chanpatterns[chan_i][patt_i]
            end
        end

        next subchans
    end

    # phew, we digested all the data.
    # now we just have to spew out some spin

    instrument_names = ['instr_dummy']

    pattern_spin = String.new
    all_patterns = channels.reduce([]){|arr,ch|arr.concat ch.patterns}.uniq!
    all_parts = all_patterns.flat_map do |patt|
        if patt.parts.size == 1
            [[patt.name,patt.parts[0]]]
        else
            patt.parts.map.with_index{|part,i|["%s_part%d" % [patt.name,i],part]}
        end
    end.to_h
    all_patterns.each do |patt|
        d = patt.parts.sum &:total_delay
        warn <<~WARN unless d == ticks_per_pattern
        Pattern #{patt.name} has #{d} ticks instead of #{ticks_per_pattern} (before optimization)
        WARN
    end
    all_parts.values.each(&:optimize)
    all_patterns.each do |patt|
        d = patt.parts.sum &:total_delay
        warn <<~WARN unless d == ticks_per_pattern
        Pattern #{patt.name} has #{d} ticks instead of #{ticks_per_pattern} (after optimization)
        WARN
    end
    part_buf = all_parts.dup
    until part_buf.empty?
        some_pattern = part_buf.first[1]
        names = part_buf.select{|k,v|v.notes==some_pattern.notes}.keys # Find all parts that have the same deltas
        pattern_spin << names.join("\n")
        pattern_spin << "\n"
        some_pattern.notes.map do |note|
            pattern_spin << "                        byte "
            delta =note.delta
            delay =note.delay
            case delta
            when (1..12)
                pattern_spin << "((12 +%2d) << 3)" % delta
            when 0
                pattern_spin << "((12    ) << 3)"
            when (-12..-1)
                pattern_spin << "((12 -%2d) << 3)" % -delta
            when nil
                pattern_spin << "___            "
            else
                pattern_spin << "TODO {#{delta.inspect}}"
            end
            first_delay = [8,delay].min
            pattern_spin << " | #{first_delay-1}"
            pattern_spin << "' pitchbend #{note.pitchbend}" if (note.pitchbend||0) != 0
            pattern_spin << "' volume #{note.volume}" if (note.volume||100) != 100
            pattern_spin << "\n"
            delay -= first_delay
            until delay == 0
                d = [8,delay].min
                pattern_spin << "                        byte ___             | #{d-1}\n"
                delay -= d
            end
        end
        pattern_spin << "                        byte END\n\n"
        names.each{|k|part_buf.delete k}
    end

    
    instruments.each do |instr|
        if all_patterns.any?{|p|p.instrument==instr}
            if instr.second_pitch == 0
                instr.index = instrument_names.size
                instrument_names << instr.name+"_A"
                instrument_names << instr.name+"_B"
            else
                instrument_names <<  instr.name
            end
        end
    end
    
    instrumentdef_spin = instrument_names.map do |i_name|
        next <<~INSTR_TEMPLATE
        ' TODO
        #{i_name}
            long    TODO_INSTR1,TODO_INSTR2
        
        INSTR_TEMPLATE
    end.join

    instrumentlist_spin = instrument_names.map do |i_name|
        "              word @#{i_name}  -(@B-RLOC)\n"
    end.join

    pattseq_spin = String.new
    channels.each do |chan|
        pattseq_spin << chan.name
        pattseq_spin << "\n"
        pitch_now = nil
        prev_instr_id = -69
        chan.patterns.each do |patt|
            instr = patt.instrument
            instr_id = nil
            if instr
                instr_name = instr.name
                instr_name += (chan.subchan_id.even? ? "_A" : "_B") if instr.second_pitch == 0
                instr_id = instrument_names.index instr_name
            end
            instr_name = ""
            patt.parts.each_with_index do |part,part_i|
                pattseq_spin << "  word "
                partname = patt.parts.size == 1 ? patt.name : ("%s_part%d" % [patt.name,part_i])
                startpitch = part.startpitch
                if startpitch
                    raise unless instr_id
                    if pitch_now != startpitch || instr_id != prev_instr_id
                        octave, o_note = note2retro(startpitch)
                        pattseq_spin << "(OCTAVE *%2d) | (NOTE *%2d)               | (INSTR *%2d) |3,    " % [octave,o_note,instr_id]
                    else
                        pattseq_spin << "                                                             "
                    end
                    pitch_now = part.last_pitch
                elsif instr_id && instr_id != prev_instr_id
                    pattseq_spin << "                            DUMMY_NOTESET | (INSTR *%2d) |3,  " % instr_id
                    pitch_now = part.last_pitch(69) # DUMMY_NOTESET sets A4
                else
                    pattseq_spin <<     "                                                             "
                end
                pattseq_spin << "(@#{partname}  -(@B-RLOC))<<1\n"
                prev_instr_id = instr_id if instr_id
            end
        end
        pattseq_spin << "\n\n"
    end


    return <<~TEMPLATE
    '──────────────────────────────────────────────────────────────────────────────────────────
    ''        Generated by BeepBox2Spin (and very likely manually edited afterwards)
    '──────────────────────────────────────────────────────────────────────────────────────────
    PUB melody
      return @MUSIC

    OBJ
    header : "retronitus_header"

    DAT
    '──────────────────────────────────────────────────────────────────────────────────────────
    '──────────────────────────────────────────────────────────────────────────────────────────
    '                                     Music header
    '──────────────────────────────────────────────────────────────────────────────────────────
    '──────────────────────────────────────────────────────────────────────────────────────────
                byte "-RETROSNDTITLE-"
                byte "TODO",0
                byte "-RETROSNDCOMMENT-"
                byte "TODO",0
                byte "-RETROSNDAUTHOR-"
                byte "TODO",0
                byte "-RETROSNDCHANNELS-"
                byte long header#WAVE_SQUARE | header#PAN_CENTER
                byte long header#WAVE_SQUARE | header#PAN_CENTER
                byte long header#WAVE_SQUARE | header#PAN_CENTER
                byte long header#WAVE_SQUARE | header#PAN_CENTER
                byte long header#WAVE_SQUARE | header#PAN_CENTER
                byte long header#WAVE_SQUARE | header#PAN_CENTER
                byte long header#WAVE_SQUARE | header#PAN_CENTER
                byte long header#WAVE_SQUARE | header#PAN_CENTER

                long 1
                                            
                byte "-RETROSNDHEAD-"
    MUSIC         word  0
    '──────────────────────────────────────────────────────────────────────────────────────────
    B '' song base
    SEQ_INIT
                  word TODO_CHANNEL
                  word TODO_CHANNEL
                  word TODO_CHANNEL
                  word TODO_CHANNEL
                  word TODO_CHANNEL
                  word TODO_CHANNEL
                  word TODO_CHANNEL
                  word TODO_CHANNEL
    '──────────────────────────────────────────────────────────────────────────────────────────
    INS_INIT
    #{instrumentlist_spin}
                  word  END 

    DAT
    '──────────────────────────────────────────────────────────────────────────────────────────
    '──────────────────────────────────────────────────────────────────────────────────────────
    '                               Pattern sequency data
    '──────────────────────────────────────────────────────────────────────────────────────────
    '──────────────────────────────────────────────────────────────────────────────────────────
    #{pattseq_spin}

    word $fffe ' end of pattseqs

    DAT
    '──────────────────────────────────────────────────────────────────────────────────────────
    '                                  Pattern data
    '──────────────────────────────────────────────────────────────────────────────────────────
    #{pattern_spin}
    DAT
    '──────────────────────────────────────────────────────────────────────────────────────────
    '──────────────────────────────────────────────────────────────────────────────────────────
    '                                 Instrument data
    '──────────────────────────────────────────────────────────────────────────────────────────
    '──────────────────────────────────────────────────────────────────────────────────────────
    org
    #{instrumentdef_spin}

    DAT
    byte "-RETROSNDFOOT-"

    CON
        END         = $0
        OCTAVE      = 1<<12
        NOTE        = 1<<8
        INSTR       = 1<<3
        SET_TEMPO   = 32768
        ___         = 25 << 3      ' Void note
        TRG_0       = 12 << 3
        TRG_1       = 26 << 3
        TRG_2       = 27 << 3  
        TRG_3       = 28 << 3  
        TRG_4       = 29 << 3  
        TRG_5       = 30 << 3  
        TRG_6       = 31 << 3  
        ms          = 67108864
        Hz          = ms / 1000
        seconds     = Hz   
        repeats     = $10 ' Not actually a single repeat, but 8(?) repeats
        delta       = 12
        wait_ms     = repeats ' Not actually a millisecond
        STEPS       = 8
        VOL         = 1 << 24
        BPM         = 366_048 / 16 ' (2^32 / 64_000 / 11) * 16

        DUMMY_NOTESET = (OCTAVE *6) | (NOTE *9) ' A4
        
        ' Instructions
        JUMP        = MODIFY|PROGRAM_CNT
        SET         = %0000
        MODIFY      = %1000
        WAIT        = MODIFY
        
        ' Registers
        FREQUENCY   = $0
        PHASE       = $1
        MODULATION  = $2
        MODPHASE    = $3
        ENVELOPE    = $4
        VOLUME      = $5
        ENVELOPE_NOW= $6
        PROGRAM_CNT = $7
            
    
        ' Masks
        LSB9         = %00000000_00000000_00000001_11111111
        ILSB9        = %11111111_11111111_11111110_00000000

        ' Gunk
        TODO_INSTR1 = JUMP
        TODO_INSTR2 = -1 * STEPS
        TODO = ___
        TODO_CHANNEL = 0

        RLOC = 0 ' TODO: relocate

    TEMPLATE

end

def self.fromString(string,**args)
    require 'json' # load JSON parser
    jsondata = JSON.parse(string,symbolize_names: true)
    return convert jsondata,**args
end

def self.note2retro(pitch)
    octave = 11 - (pitch)/12 # Retronitus' weird octave system
    o_note = (pitch)%12
    return octave,o_note
end
# These two are just here so I don't loose them
def self.retro_to_freq(octave,o_note)
    ((1000000000.0 * (0.9438743967**(11-o_note)) *  (67878.79 / 64_000)).to_i >> octave) / (2.0**32 / 64_000.0)
end
def self.note2freq(pitch)
    retro_to_freq(*note2retro(pitch))
end

private
def self.push_noteon(chan_i,channelparts,current_parts,pitch,event_length,volume=100,pitchbend=0) 
    raise "Non-integral event length (#{event_length}) !" unless event_length % 1 == 0
    event_length = event_length.to_i
    melodic = pitch.kind_of?(Integer)
    chan_part = current_parts[chan_i]
    new_part = false
    if chan_part.nil?
        new_part = true
    elsif melodic && chan_part.startpitch.nil?
        chan_part.startpitch = pitch
    elsif !chan_part.valid_pitch?(pitch,event_length)
        new_part = true
    end
    if new_part
        channelparts[chan_i] << chan_part if chan_part
        chan_part = PatternPart.new(melodic ? pitch : nil,[])
        current_parts[chan_i] = chan_part
    end
    delta = melodic ? pitch - chan_part.last_pitch : pitch
    chan_part << Note.new(delta,event_length,pitchbend,volume)
end

end # module end


if __FILE__ == $0

    require 'optparse'

    tickdiv = 1

    opts = OptionParser.new
    opts.on("--tdiv N","Divide timing by N") do |n|
        tickdiv = Integer(n)
    end
    opts.parse!

    case ARGV.size
    when 1
        puts "Debug mode: printing result!"
        puts BeepBox2Spin.fromString(File.read(ARGV[0]),tickdiv_in:tickdiv)
    when 2
        raise "File already exists" if File.exist? ARGV[1]
        begin
            outfile = File.open ARGV[1],'w:UTF-16'
            outfile.write BeepBox2Spin.fromString(File.read(ARGV[0]),tickdiv_in:tickdiv)
        ensure
            outfile.close
        end
    else puts "Usage: beepbox2spin [input file] [output file]"
    end

end
