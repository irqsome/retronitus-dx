# encoding: utf-8
# frozen_string_literal: true
require_relative 'rsndhelper'
module BlasterShot
LICENSE = <<-LICENSE
┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│         Retronitus DX and BlasterShot - (C) 2009-16 Johannes Ahlebrand, (C) 2020 IRQsome Software/Ada Gottensträter          │                                                            
├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
│                                    TERMS OF USE: Parallax Object Exchange License                                            │                                                            
├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
│Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    │ 
│files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    │
│modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software│
│is furnished to do so, subject to the following conditions:                                                                   │
│                                                                                                                              │
│The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.│
│                                                                                                                              │
│THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          │
│WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         │
│COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   │
│ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
LICENSE

    def self.default_phaseacc(i) <<~PHASEACC end
              add c#{i}_phase,c#{i}_freq
    PHASEACC
    def self.carry_phaseacc(i) <<~PHASEACC2 end
              add c#{i}_phase,c#{i}_freq wc
    PHASEACC2
    def self.default_modulate(i) <<~MODULATE end
              test c#{i}_modspeed,#511 wz
        if_nz movi c#{i}_modphase,c#{i}_modspeed
        if_z  add  c#{i}_modphase,c#{i}_modspeed
    MODULATE
    def self.default_envelope(i) <<~ENVELOPE end
              add  c#{i}_envelope,c#{i}_ASD
              test c#{i}_envelope,volumeBits wz
        if_z  movi c#{i}_envelope,c#{i}_ASD
              max  c#{i}_envelope,c#{i}_vol
    ENVELOPE
    def self.default_stuff(i)
        default_phaseacc(i)+default_modulate(i)+default_envelope(i)
    end

    PANREGS = {
        center: 'outC',
        left: 'outL',
        right: 'outR',
        surround: 'outS',
    }

    def self.gimme_retronitus(channels,output_type=:stereo)
        template = File.read(File.join(File.dirname(__FILE__), "./blastershot_template.spin"),encoding: Encoding::ISO_8859_15)
        outstr = "".dup
        parts = {}
        %i[header spin common1 common2 common3].each do |part|
            template =~ /{{<#{part}>}}(.*){{<\/#{part}>}}/m
            parts[part] = $1
        end
        outstr << parts[:header]
        outstr << parts[:spin]
        outstr << parts[:common1]
        # Init code
        case output_type
        when :stereo
            outstr << <<~STEREO
                mov ctra, tempValue1                     
                mov ctrb, tempValue2  
                mov frqa, val31bit
                mov frqb, val31bit                  
                mov dira, tempValue3
            STEREO
        when :dualmono
            outstr << <<~DUALMONO
                mov ctra, tempValue1                     
                mov ctrb, tempValue2  
                mov frqa, val31bit
                mov frqb, val31bit                  
                mov dira, tempValue3
            DUALMONO
        when :mono
            outstr << <<~MONO  
                mov monoCenter,tempValue2
                andn monoCenter,#31
                mov monoAttenuation,tempValue2
                mov ctra, tempValue1
                mov frqa, monoCenter
                mov dira, tempValue3
            MONO
        else raise "Unknown output type (must be stereo, dualmono or mono)"
        end
        outstr << parts[:common2]
        allocregs = []
        # Per-channel code
        (1..channels.size).each do |i|
            chan = channels[i-1]
            raise unless chan.is_a? Retronitus::Channel
            p chan.wave

            mixr = (output_type != :stereo) ? 'outC' : PANREGS[chan.pan]
            vol = chan.volume

            case chan.wave
            when :none 
                outstr << <<~NONE
                ch#{i}_None
                      ' NO WAVE
                NONE
            when :square
                outstr << <<~SQUARE
                ch#{i}_Square
                      #{default_stuff(i)}
                      cmp  c#{i}_modphase,c#{i}_phase wc
                      mov  tempValue1,c#{i}_envelope
                      shr  tempValue1,##{4-vol}
                      sumc #{mixr},tempValue1
                SQUARE
            when :sawtooth
                outstr << <<~SAWTOOTH
                ch#{i}_Sawtooth
                #{default_stuff(i)}
                      mov  tempValue1,c#{i}_phase wc
                      abs  tempValue2,c#{i}_modphase
                      sumc tempValue1,tempValue2 wc
                      mov  tempValue2,c#{i}_envelope
                      #{"sar  tempValue1,##{9-vol}" unless vol == 0}
                      call ##{vol==0 ? "sar9Multiply" : "multiply"}
                      add  #{mixr},tempValue3
                SAWTOOTH
            when :triangle
                outstr << <<~TRIANGLE
                ch#{i}_Triangle
                #{default_phaseacc(i)}
                #{default_envelope(i)}
                      abs  tempValue1,c#{i}_phase
                      and  tempValue1,c#{i}_modspeed
                      sub  tempValue1,val30bit
                      sar  tempValue1,##{8-vol}
                      mov  tempValue2,c#{i}_envelope
                      call #multiply
                      add  #{mixr},tempValue3
                TRIANGLE
            when :oldnoise
                outstr << <<~OLDNOISE
                ch#{i}_Noise
                #{default_phaseacc(i)}
                #{default_envelope(i)}
                      cmp  c#{i}_phase,c#{i}_freq wc
                if_c  ror  c#{i}_modphase,#15
                if_c  add  c#{i}_modphase,c#{i}_modspeed
                      mov  tempValue1,c#{i}_modphase
                      mov  tempValue2,c#{i}_envelope
                      #{"sar  tempValue1,##{9-vol}" unless vol == 0}
                      call ##{vol==0 ? "sar9Multiply" : "multiply"}
                      add  #{mixr},tempValue3
                OLDNOISE
            when :lfsr
                outstr << <<~LFSR
                ch#{i}_LFSR
                #{carry_phaseacc(i)}
                      muxc :selfmod#{i},#1
                #{default_envelope(i)}
                      test c#{i}_modphase,c#{i}_modspeed wc
                :selfmod#{i}
                      rcr c#{i}_modphase,#0-0 wc
                      mov  tempValue1,c#{i}_envelope
                      shr  tempValue1,##{4-vol}
                      sumc #{mixr},tempValue1
                LFSR
            when :dpcm
                outstr << <<~DPCM
                ch#{i}_DPCM
                      tjz c#{i}_modspeed,#ch#{i}_DPCM_noload
                      mov c#{i}_modphase,c#{i}_modspeed
                      add c#{i}_modphase,waveTablePointer#{i}
                      mov c#{i}_modspeed,#0
                      mov c#{i}_phase,#0
                ch#{i}_DPCM_noload
                #{carry_phaseacc(i)}
                if_c  cmpsub c#{i}_vol,#1 wc,wz
                if_nc jmp #ch#{i}_DPCM_nobit
                      rdlong tempValue1,c#{i}_modphase
                      test c#{i}_vol,#31 wz
                if_z  add c#{i}_modphase,#4
                      shl tempValue1,c#{i}_vol
                      shl tempValue1,#1 nr,wc
                      sumnc c#{i}_envelope,c#{i}_ASD
                ch#{i}_DPCM_nobit
                      add #{mixr},c#{i}_envelope
                DPCM
            when :stereosquare
                if output_type == :stereo
                    outstr << <<~STEREOSQUARE
                    ch#{i}_StereoSquare
                    #{default_phaseacc(i)}
                    #{default_modulate(i)}
                        cmp  c#{i}_modphase,c#{i}_phase wc
                        sumc #{PANREGS[:left]},c#{i}_vol
                        sumc #{PANREGS[:right]},c#{i}_envelope
                    STEREOSQUARE
                else
                    outstr << <<~MONOSQUARE
                    ch#{i}_NotQuiteStereoSquare
                    #{default_phaseacc(i)}
                    #{default_modulate(i)}
                        cmp  c#{i}_modphase,c#{i}_phase wc
                        mov  tempValue1,c#{i}_vol
                        add  tempValue1,c#{i}_envelope
                        sar  tempValue1,#1
                        sumc #{mixr},tempValue1
                    MONOSQUARE
                end
            when :filtersquare 
                outstr << <<~FILTERSQUARE
                ch#{i}_FilterSquare
                #{default_stuff(i)}
                      cmp  c#{i}_modphase,c#{i}_phase wc
                      mov  tempValue1,c#{i}_envelope
                      shr  tempValue1,##{4-vol}
                      test c#{i}_vol,#1<<5 wz
                if_nz sumc   #{mixr},tempValue1 
                      sumnc tempValue1,c#{i}_filterval
                      sar  tempValue1,c#{i}_vol
                      sumc c#{i}_filterval,tempValue1
                      sumnz #{mixr},c#{i}_filterval
                FILTERSQUARE
                allocregs << "c#{i}_filterval"
            end

        end
        # Mixer
        case output_type
        when :stereo
            outstr << <<~STEREO
            mixer
                add outL,outC
                add outR,outC
                add outL,outS
                sub outR,outS
                waitcnt cnt, sampleRate
                mov frqa,outL
                mov frqb,outR
                mov outL,val31bit
                mov outR,val31bit
                mov outC,#0
                mov outS,#0
                jmp #mainLoop
            outL long 0
            outR long 0
            outS long 0
            STEREO
        when :dualmono
            outstr << <<~DUALMONO
            mixer
                waitcnt cnt, sampleRate
                mov frqa,outC
                mov frqb,outC
                mov outC,val31bit
                jmp #mainLoop
            DUALMONO
        when :mono
            outstr << <<~MONO
            mixer
                sar outC,monoAttenuation
                add outC,monoCenter
                waitcnt cnt, sampleRate
                mov frqa,outC
                mov outC,#0
                jmp #mainLoop
            monoAttenuation long 0
            monoCenter long 0
            MONO
        else raise
        end

        allocregs.each do |reg|
            outstr << "#{reg} long 0\n"
        end

        outstr << parts[:common3]

        outstr
    end

end

if __FILE__ == $0
    if ARGV.size != 3
        puts <<~USAGE
            BlasterShot!
            generate a Retronitus DX cog image.
            Usage:
            blastershot [compiled song file] [output type] [assembly output file]

            [output type] must be "stereo","dualmono" or "mono"

            You can also "require" this for use as a library
        USAGE
        exit
    end
    bindata = File.binread ARGV[0]
    song = Retronitus.parse_binary bindata
    File.write ARGV[2],BlasterShot.gimme_retronitus(song.channels,ARGV[1].to_sym)
end
