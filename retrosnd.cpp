#include "retrosnd.hpp"
#include "fastparity.hpp"


constexpr vmlong Retronitus::fTable[12] = {
    vmlong(MSN*nf*nf*nf*nf*nf*nf*nf*nf*nf*nf*nf),
    vmlong(MSN*nf*nf*nf*nf*nf*nf*nf*nf*nf*nf),
    vmlong(MSN*nf*nf*nf*nf*nf*nf*nf*nf*nf),
    vmlong(MSN*nf*nf*nf*nf*nf*nf*nf*nf),
    vmlong(MSN*nf*nf*nf*nf*nf*nf*nf),
    vmlong(MSN*nf*nf*nf*nf*nf*nf),
    vmlong(MSN*nf*nf*nf*nf*nf),
    vmlong(MSN*nf*nf*nf*nf),
    vmlong(MSN*nf*nf*nf),
    vmlong(MSN*nf*nf),
    vmlong(MSN*nf),
    vmlong(MSN),
};

vmlong Retronitus::mul32x6(vmlong_s arg1,vmlong arg2) {
    return arg1*(arg2>>26);
}
void Retronitus::Channel::default_phaseacc() {
    phase += frequency;
}
bool Retronitus::Channel::carry_phaseacc() {
    return __builtin_add_overflow(phase,frequency,&phase);
}
void Retronitus::Channel::default_modulate() {
    if (modspeed & 511) {
        movi(modphase,modspeed);
    } else {
        modphase += modspeed;
    }
}
void Retronitus::Channel::default_envelope() {
    envelope += asd;
    if (!(envelope&volume_bits)) {
        movi(envelope,asd);
    }
    if (envelope > volume) envelope = volume;
}
void Retronitus::Channel::doStep() {
    if((!step_ptr) || step_wait--) return;
    auto step = rdsndbyte(step_ptr);
    if (!step) return;
    step_ptr++;
    step_wait = step&7;
    auto stepnote = int(step>>3)-12;
    if (stepnote==(25-12)) {
        // Do nothing
    } else if (stepnote>(25-12)) {
        // TRGx
        trigger = noteOn_ptr-((stepnote-(25-12))<<3);
    } else {
        trigger = noteOn_ptr;
        note += stepnote;
        if (note<0) {
            note += 12;
            octave++;
        }
        if (note>=12) {
            note -= 12;
            octave--;
        }
        frequency = fTable[note] >> (octave&31);
    }
}
void Retronitus::Channel::newPattern() {
    if (!pattseq_ptr || (step_ptr && rdsndbyte(step_ptr))) return;
    auto pattseq = rdsndword(pattseq_ptr);
    if (!(pattseq&1)) { // Run pattern
        step_ptr = pattseq>>1;
        pattseq_ptr += 2;
    } else if (pattseq&2) { // Set Stuff
        note = (pattseq>>8)&15;
        octave = pattseq>>12;
        noteOn_ptr = rdsndword(instrumentsPtr+((pattseq>>2)&63));
        pattseq_ptr += 2;
    } else if (pattseq&4) { // Jump
        auto m = pattseq >> 4;
        if (pattseq & 8) pattseq_ptr -= m;
        else             pattseq_ptr += m;
    } else { // Tempo
        tempo = vmlong(pattseq>>3)<<14;
        pattseq_ptr += 2;
    }
}
void Retronitus::Channel::updateSPU() {
    if (!spu_pc || !enabled) return;
    /*if(repeat_cnt) {
        if (repeat_val) registers[repeat_reg] += repeat_val;
    } else */{
        auto opcode = rdsndlong(spu_pc);
        spu_pc += 4;
        auto data = rdsndlong(spu_pc);
        spu_pc += 4;
        auto regid = opcode&7;
        bool add = opcode&8;
        auto op_repeat = opcode >> 4;
        if (add) registers[regid] += data;
        else     registers[regid]  = data;
        if (op_repeat) {
            spu_pc -= 8;
        }
    }
}

void Retronitus::Channel::handleTrigger() {
    if (trigger) {
        spu_pc = trigger;
        trigger = 0;
        repeat_cnt = 0;
    }
    auto opcode = rdsndlong(spu_pc);
    auto op_repeat = opcode >> 4;
    auto cntWas = repeat_cnt;
    if (repeat_cnt) repeat_cnt--;
    if (cntWas == 0) {
        repeat_cnt = op_repeat;
        if (op_repeat) {
            auto regid = opcode&7;
            bool add = opcode&8;
            if (add) {
                repeat_reg = regid;
                repeat_val = rdsndlong(spu_pc+4);
            } else {
                repeat_val = 0;
            }
        }
    } else if (cntWas == 1) {
        spu_pc += 8;
    }
}

void Retronitus::Channel::waveShape(vmlong &lout,vmlong &rout) {
    if (!enabled) return;
    switch (wave) {
    case NONE: return;
    case SQUARE: {
        default_stuff();
        auto v = envelope >> (4+attenuate);
        mixIn( phase < modphase ? v : 0-v ,lout,rout);
    } break;
    case SAWTOOTH: {
        default_stuff();
        auto v = rsnd_abs(modphase);
        mixIn(mul32x6(vmlong_s(phase + (phase&(1U<<31)?0-v:v))>>(9+attenuate),envelope),lout,rout);
    } break;
    case TRIANGLE: {
        default_phaseacc();
        default_envelope();
        mixIn( mul32x6(vmlong_s((rsnd_abs(phase)&modspeed)-(1U<<30))>>(8+attenuate),envelope),lout,rout);
    } break;
    case OLDNOISE: {
        default_phaseacc();
        default_envelope();
        if (phase < frequency) {
            modphase = ((modphase >> 15)|(modphase << 17))+modspeed;
        }
        mixIn(mul32x6(vmlong_s(modphase)>>(9+attenuate),envelope),lout,rout);
    } break;
    case LFSR: {
        bool tick = carry_phaseacc();
        default_envelope();
        bool out = modphase & 1;
        if (tick) {
            modphase = (modphase>>1)|(fastparity32(modphase & modspeed) ? (1<<31) : 0);
        }
        auto v = envelope >> (4+attenuate);
        mixIn( out ? v : 0-v ,lout,rout);
    } break;
    case DPCM: {
        if (modspeed) {
            modphase = modspeed+spu_pc;
            modspeed = 0;
            phase = 0;
        }
        bool tick = carry_phaseacc();
        if (tick && volume) {
            volume--;
            vmlong bits = rdsndlong(modphase);
            if (!(volume&31)) modphase+=4;
            bool bit = (bits<<(volume&31))&0x80000000;
            envelope += bit ? asd : -asd;
        }
        mixIn(envelope,lout,rout);
    } break;
    case STEREOSQUARE: {
        default_phaseacc();
        default_modulate();
        bool high = phase < modphase;
        lout += high ? volume : 0-volume;
        rout += high ? envelope : 0-envelope;
    } break;
    case FILTERSQUARE: {
        default_stuff();
        bool high = phase < modphase;
        auto v = envelope >> (4+attenuate);
        if(volume & (1<<5)) mixIn(!high ? v : 0-v,lout,rout);
        vmlong_s diff = (high ? v : 0-v) - wave_extra;
        wave_extra += (vmlong)(diff>>(volume&31));
        mixIn((volume & (1<<5)) ? wave_extra : 0-wave_extra ,lout,rout);
    } break;
    default: return;
    }
}

void Retronitus::Channel::mixIn(vmlong sample,vmlong &lout,vmlong &rout) {
    switch (stereo?pan:LEFT) {
    case CENTER:
        lout += sample;
        rout += sample;
        break;
    case LEFT:
        lout += sample;
        break;
    case RIGHT:
        rout += sample;
        break;
    case SURROUND:
        lout += sample;
        rout -= sample;
        break;
    }
}

uint Retronitus::task,Retronitus::triggerChan;
vmlong Retronitus::tempo,Retronitus::tempoAccumulator;
vmword Retronitus::instrumentsPtr,Retronitus::pausePtr;
vmbyte Retronitus::channelUpdate;
bool Retronitus::stereo,Retronitus::stopped;
Retronitus::Channel Retronitus::channels[Retronitus::NUM_CHANNELS];

void Retronitus::doSample(vmlong &lout,vmlong &rout) {
    if (stopped) {
        lout = rout = 0;
        return;
    }
    // Handle tasks
    if (task > NUM_CHANNELS+2 || !task--) {// 10/10 Prepare music (no op)
        task = NUM_CHANNELS+2;
    } else if (task == NUM_CHANNELS+1) {    //  9/10 Handle music
        if(__builtin_add_overflow(tempo,tempoAccumulator,&tempoAccumulator)) {
            channelUpdate = 255;
        }
        bool doStep = channelUpdate &1;
        channelUpdate >>= 1;
        if (doStep) {
            channels[triggerChan].doStep();
        } else {
            channels[triggerChan].newPattern();
        }
        
    } else if (task == NUM_CHANNELS+0) {    //  8/10 Handle trigger
        if (!triggerChan--) triggerChan = 7;
        channels[triggerChan].handleTrigger();
    } else {                                //0-7/10 Handle SPUs
        channels[task].updateSPU();
    }

    // Sound synthesis
    lout = 0;
    rout = 0;
    for (uint i=0;i<NUM_CHANNELS;i++) {
        channels[i].waveShape(lout,rout);
    }
}

void Retronitus::doSampleBlock(vmlong *block,uint count) {
    for (;count!=0;count--) {
        doSample(*(block+0),*(block+1));
        block+=2;
    }
}

void Retronitus::trigger(uint channel,vmword trigger_ptr) {
    if (channel >= NUM_CHANNELS) return;
    channels[channel].trigger = trigger_ptr;
}

void Retronitus::init(bool stereo,vmword song_ptr,vmword pause_ptr) {
    Retronitus::stereo = stereo;
    Retronitus::instrumentsPtr = song_ptr + NUM_CHANNELS*2;

    for (uint i=0;i<NUM_CHANNELS;i++) {
        auto &ch = channels[i];
        ch = {};
        ch.enabled = true;
        ch.wave = SQUARE;
        ch.pan = CENTER;
        ch.spu_pc = 0;
        for (uint j=0;j<8;j++) ch.registers[j] = 0;
        ch.pattseq_ptr = rdsndword(song_ptr+(i*2));
    }
    channels[7].wave = LFSR;
    pausePtr = pause_ptr;
    stopped = true;
}

void Retronitus::start() {
    stopped = false;
}
void Retronitus::stop() {
    stopped = true;
}
