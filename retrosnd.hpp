#pragma once
#include "retrosnd_env.hpp"

namespace Retronitus {

constexpr inline vmlong rsnd_abs(vmlong n) {
    return n>>31?0-n:n;
}

constexpr vmlong volume_bits = 0xfc000000;

constexpr int SAMPLE_RATE = 64000;//Hz
constexpr float nf = 0.9438743967f;
constexpr float MSN = 2000000000.f * (67878.79f / SAMPLE_RATE);
extern const vmlong fTable[12];

constexpr inline void movi(vmlong &d,vmlong s) {
    d &= ~(511U<<23);
    d |= s<<23;
}

vmlong mul32x6(vmlong_s arg1,vmlong arg2);

enum Waveform : vmbyte {
    NONE         = 0,
    SQUARE       = 1,
    SAWTOOTH     = 2,
    TRIANGLE     = 3,
    OLDNOISE     = 4,
    LFSR         = 5,
    DPCM         = 6,
    STEREOSQUARE = 7,
    FILTERSQUARE = 8,
};
enum Panning : vmbyte {
    CENTER  = 0,
    LEFT    = 1,
    RIGHT   = 2,
    SURROUND= 3,
};

struct Channel {
    bool enabled;
    Waveform wave;
    Panning pan;
    vmbyte repeat_reg,step_wait,octave;
    vmbyte_s note,attenuate;
    vmword step_ptr,pattseq_ptr,noteOn_ptr,trigger;
    union {
        vmlong registers[8];
        struct {
            // envelope is current envelope level
            // volume is max. envelope level
            // asd is envelope speed and reset value
            vmlong frequency,phase,modspeed,modphase,asd,volume,envelope,spu_pc;
        };
    };
    vmlong wave_extra;
    vmlong repeat_cnt,repeat_val;

    void default_phaseacc();
    bool carry_phaseacc();
    void default_modulate();
    void default_envelope();
    inline void default_stuff() {
        default_phaseacc();
        default_modulate();
        default_envelope();
    }
    void doStep();
    void newPattern();
    void updateSPU();
    void handleTrigger();
    void waveShape(vmlong &lout,vmlong &rout);
    void mixIn(vmlong sample,vmlong &lout,vmlong &rout);
};

extern uint task,triggerChan;
extern vmlong tempo,tempoAccumulator;
extern vmword triggerPtr,instrumentsPtr,pausePtr;
extern vmbyte channelUpdate;
extern bool stereo,stopped;
constexpr uint NUM_CHANNELS = 8;
extern Channel channels[NUM_CHANNELS];

void doSample(vmlong &lout,vmlong &rout);
void doSampleBlock(vmlong *block,uint count);
void trigger(uint channel,vmword trigger_ptr);
void init(bool stereo,vmword song_ptr,vmword pause_ptr);
void start();
void stop();

}