#pragma once
#include <cstdint>
#include <cstdio>

typedef uint32_t vmlong;
typedef int32_t vmlong_s;
typedef uint16_t vmword;
typedef int16_t vmword_s;
typedef uint8_t vmbyte;
typedef int8_t vmbyte_s;
typedef unsigned int uint;

extern vmbyte retrosnd_hub[0x10000];

#if __BYTE_ORDER__ != __ORDER_LITTLE_ENDIAN__
#error "Currently only little endian"
#endif

inline vmlong rdsndlong(vmlong addr) {
    addr &= 0xfffc;
    vmlong data = *((vmlong*)(retrosnd_hub+addr));
    return data;
}
inline vmword rdsndword(vmlong addr) {
    addr &= 0xfffe;
    vmword data = *((vmword*)(retrosnd_hub+addr));
    return data;
}
inline vmbyte rdsndbyte(vmlong addr) {
    addr &= 0xffff;
    vmbyte data = *(retrosnd_hub+addr);
    return data;
}
