# encoding: utf-8
# frozen_string_literal: true

module Retronitus
    WAVEFORMS = %i[none square sawtooth triangle oldnoise lfsr dpcm stereosquare filtersquare]
    PANNINGS = %i[center left right surround]

    Song = Struct.new(:data,:title,:comment,:author,:channels,:nosfx,keyword_init:true) do
        def self.parse_binary(string)
            Retronitus.parse_binary(string)
        end
        
        def relocate(reloc)
            words = data.unpack("S<*")
            # reloc channels
            8.times do |i|
                words[i] += reloc if words[i] != 0
            end
            # reloc instruments
            ptr = 8
            until words[ptr] == 0
                words[ptr] += reloc
                ptr+=1
            end
            ptr+=1
            # reloc patterns
            until words[ptr] == 0xFFFE
                words[ptr] += reloc<<1 unless words[ptr] & 1 == 1
                ptr+=1
            end
            self.data = words.pack("S<*")
        end

        def rsx_metadata
            # TODO: Validate (i.e. make sure there aren't any invalid characters)
            {
                "TITL" => self.title,
                "AUTH" => self.author,
                "COMM" => self.comment,
            }
            .transform_values{|v|v.encode(Encoding::ISO_8859_15)}
            .reject{|k,v|v.nil? || v.empty?}
        end

        def display_metadata
            {
                title:   self.title   || "No title",
                comment: self.comment || "No comment",
                author:  self.author  || "Unknown",
            }
        end

    end
    Channel = Struct.new(:wave,:volume,:pan)

    PRESET_CHANNELS = [
        Channel[:square,0,:center]  .freeze,
        Channel[:square,0,:center]  .freeze,
        Channel[:square,0,:center]  .freeze,
        Channel[:sawtooth,0,:center].freeze,
        Channel[:sawtooth,0,:center].freeze,
        Channel[:sawtooth,0,:center].freeze,
        Channel[:triangle,0,:center].freeze,
        Channel[:oldnoise,0,:center].freeze,
    ].freeze

    def self.parse_binary(string)
        raise unless string.encoding == Encoding::ASCII_8BIT

        unless string =~ /-RETROSNDHEAD-\0\0(.*?)-RETROSNDFOOT-/mn
            raise ArgumentError,"Song data not found"
        end
        data = $1

        title = nil
        if string =~ /-RETROSNDTITLE-(.*?)\0/mn
            title = $1.force_encoding(Encoding::ISO_8859_15)
        end

        comment = nil#"No comment"
        if string =~ /-RETROSNDCOMMENT-(.*?)\0/mn
            comment = $1.force_encoding(Encoding::ISO_8859_15)
        end

        author = nil#"Unknown"
        if string =~ /-RETROSNDAUTHOR-(.*?)\0/mn
            author = $1.force_encoding(Encoding::ISO_8859_15)
        end

        nosfx = !!(string =~ /-RETROSNDNOSFX-/mn)

        channels = PRESET_CHANNELS
        if string =~ /-RETROSNDCHANNELS-(.{32})/mn
            channels = (0..7).map do |i|
                #puts "%08X" % $1.slice(i*4,4).unpack("<I")
                wave,pan,amplify = $1.slice(i*4,4).unpack("CCxc")
                #p "W: #{wave} P: #{pan} A: #{amplify}"
                Channel.new(WAVEFORMS[wave],amplify,PANNINGS[pan])
            end
        end

        return Song[data:data,title:title,comment:comment,author:author,channels:channels,nosfx:nosfx]
    end

    def self.parse_sfx_binary(string)
        raise unless string.encoding == Encoding::ASCII_8BIT
        return string.to_enum(:scan,/-RETROSNDSFXHEAD-(.*?)\0*?\x80(.*?)-RETROSNDSFXFOOT-/mn).to_h.transform_keys{|k|k.dup.force_encoding(Encoding::ISO_8859_15)}
    end

    def self.parse_sfxlist(string)
        string.each_line.map(&:strip).reject{|l|l.start_with? /(#|\W)/}
    end
end