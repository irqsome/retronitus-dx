# encoding: utf-8
# frozen_string_literal: true
require 'open3'
require 'fiddle'
require 'fiddle/import'

require_relative 'rsndhelper'

module Retronitus
    FUNCTIONS = {
        init: 'void _ZN10Retronitus4initEbtt(bool,vmword,vmword)',
        start: 'void _ZN10Retronitus5startEv()',
        stop: 'void _ZN10Retronitus4stopEv()',
        trigger: 'void _ZN10Retronitus7triggerEjt(unsigned int,vmword)',
        doSample: 'void _ZN10Retronitus8doSampleERjS0_(unsigned int*,unsigned int*)',
        doSampleBlock: 'void _ZN10Retronitus13doSampleBlockEPjj(unsigned int*,unsigned int)'
    }
    extend Fiddle::Importer
    dlload "#{__dir__}/libretronitus.dll"
    typealias "uint","unsigned int"
    typealias "bool","unsigned char"
    typealias "vmlong","unsigned int"
    typealias "vmword","unsigned short"
    typealias "vmbyte","unsigned char"
    typealias "vmbyte_s","signed char"
    FUNCTIONS.each_pair do |sym,signature|
        singleton_class.send :alias_method,sym,extern(signature).name.to_sym
    end
    ChannelStruct = struct <<~CHANNEL
    bool enabled;
    vmbyte wave;
    vmbyte pan;
    vmbyte repeat_reg;
    vmbyte step_wait;
    vmbyte octave;
    vmbyte_s note;
    vmbyte_s attenuate;
    vmword step_ptr;
    vmword pattseq_ptr;
    vmword noteOn_ptr;
    vmword trigger;
        struct {
            vmlong frequency;
            vmlong phase;
            vmlong modspeed;
            vmlong modphase;
            vmlong asd;
            vmlong volume;
            vmlong envelope;
            vmlong spu_pc;
        };
    vmlong wave_extra;
    vmlong repeat_cnt;
    vmlong repeat_val;
    CHANNEL
    Channels = []
    ChArr = import_symbol('_ZN10Retronitus8channelsE')
    8.times do |i|
        Channels << ChannelStruct.new(ChArr+ChannelStruct.size*i)
    end
    Hub = Fiddle::Pointer.new(import_symbol('retrosnd_hub'))
end

def Retronitus.play_to(stream,block_size=512,length=Float::INFINITY)
    raise unless block_given?
    buf = (?\0*(8*block_size)).b
    ptr1 = Fiddle::Pointer[buf]
    ptr2 = ptr1 + 4
    min = 0
    max = 0
    i = 0
    begin
        loop do
=begin
            block_size.times do
                i+=1
                Retronitus.doSample(ptr1,ptr2)
                stream.write buf
                ls,rs = *buf.unpack("l<2")
                max = [max,ls,rs].max
                min = [min,ls,rs].min
                break if i >= length
            end
=end
            Retronitus.doSampleBlock(ptr1,block_size)
            stream.write buf
            i+=block_size

            buf.unpack("l<*").each_slice(2) do |ls,rs|
                max = [max,ls,rs].max
                min = [min,ls,rs].min
            end

            break if i >= length
            break unless yield
        end
    rescue => exception
        puts exception.full_message
    end
    return min,max
end

def Retronitus.ffplay(block_size=512,length=Float::INFINITY,&block)
    Open3.pipeline_w('ffmpeg -f s32le -ar 64000 -ac 2 -i - -filter_complex "[0:a] showwaves=s=640x240:mode=p2p:r=60:split_channels=1,format=bgr24 [scope];[0:a] avectorscope=s=320x240:r=60,format=bgr24 [vector];[0:a] showspectrum=s=320x240:slide=scroll,format=bgr24 [spec];[spec][vector]hstack [toprow];[toprow][scope]vstack,format=bgr24[vis]" -map "[vis]" -map 0:a -pixel_format bgr24 -f matroska -allow_raw_vfw 1 -c:v rawvideo -c:a copy -',
        'ffplay -') do |stdin, wait_thr|
        return play_to(stdin,block_size,length,&block)
    end
end

if __FILE__ == $0
    require 'optparse'

    opts = OptionParser.new

    writefile = nil
    opts.on("-w","--write FILE") do |f|
        writefile = f
    end
    playlength = Float::INFINITY
    opts.on("-t","--time N") do |n|
        playlength = Integer(n)
    end
    solochannels = []
    opts.on("-c","--channel N") do |n|
        n.each_char do |c|
            if (?0..?7).include? c
                solochannels << Integer(c)
            else raise ArgumentError,"#{c.inspect} is not a valid channel"
            end
        end
    end
    opts.parse!

    if ARGV.size == 0
        puts <<~USAGE
            Retronitus DX command-line player.
            Usage:
            rsndplay [file]

            You can also "require" this for use as a library
        USAGE
        exit
    end

    bindata = File.binread ARGV[0]
    song = Retronitus.parse_binary bindata
    Retronitus::Hub[0,[0xffff,song.data.size].min] = song.data
    Retronitus.init(1,0,0x7FFF)
    Retronitus.start

    (0..7).each do |i|
        enable = !!song.channels[i] && (solochannels.empty? || solochannels.include?(i))
        puts "Channel #{i} disabled!" unless enable
        Retronitus::Channels[i].enabled = enable ? 1 : 0
        if enable
            Retronitus::Channels[i].wave = Retronitus::WAVEFORMS.index song.channels[i].wave
            Retronitus::Channels[i].attenuate = 0 - song.channels[i].volume
            Retronitus::Channels[i].pan = Retronitus::PANNINGS.index song.channels[i].pan
        end
    end

    if writefile
        File.open(writefile,'wb') do |stream|
                Retronitus.play_to(stream,512,playlength) {| |true}
        end
    else
        min,max = Retronitus.ffplay(512,playlength) {| |true}
        dispmeta = song.display_metadata
        puts "Title  : #{dispmeta[:title]}"
        puts "Author : #{dispmeta[:author]}"
        puts "Comment: #{dispmeta[:comment]}"
        puts "Min: %08X Max: %08X" % [min,max]
        headroom = 0x7FFF_FFFF - [min,max].map(&:abs).max
        puts "Headroom: %08X" % [headroom]
    end
end