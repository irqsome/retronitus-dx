# encoding: utf-8
# frozen_string_literal: true
require_relative 'rsndhelper'
require_relative 'blastershot'
module RSX_Compiler

    DEFAULT_HOMESPUN_PATH = "homespun"
    SONG_PAGES = 12
    SONG_LIMIT = SONG_PAGES*512
    COG_PAGES = 4
    COG_LIMIT = 4*(512-16)
    EFFECT_PAGES = 1
    EFFECT_LIMIT = EFFECT_PAGES*512
    META_PAGES = 1
    META_LIMIT = 1*512

    def self.compile_rsx(output_type,reloc,sfxlist,sfxhash,*songlist,homespun_path:DEFAULT_HOMESPUN_PATH)
        have_metadata = !songlist.all?{|s|s.rsx_metadata.empty?}
        songs = songlist.map.with_index do |song,i|
            song = song.dup
            song.relocate(reloc)
            nosfx = song.nosfx
            datalimit = nosfx ? SONG_LIMIT : SONG_LIMIT - 512
            raise "Song #{song.title.inspect} at index #{i} too big" if song.data.size > datalimit
            songdata_padded = song.data.ljust(SONG_LIMIT,?\0)
            code = BlasterShot.gimme_retronitus(song.channels,output_type)
            cogimg = compile_cogimage(code,homespun_path:homespun_path)
            raise "Song #{song.title.inspect} at index #{i} generates too big cog image (HOW?)" if cogimg.size > COG_LIMIT
            cogimg_padded = cogimg.ljust(COG_LIMIT,?\0)
            cogimg_padded << [
                *song.channels.flat_map do |ch|
                    [Retronitus::WAVEFORMS.index(ch.wave),Retronitus::PANNINGS.index(ch.pan),0,ch.volume&255]
                end,
                nosfx ? 1 : 0,
            ].pack("C32 x31 C")
            raise unless cogimg_padded.size == COG_PAGES*512
            next cogimg_padded+songdata_padded
        end
        effects = sfxlist.map.with_index do |effectname,i|
            effectdata = sfxhash[effectname]
            raise "Effect #{effectname.inspect} at index #{i} does not exist" if effectdata.nil?
            raise "Effect #{effectname.inspect} at index #{i} too big" if effectdata.size > EFFECT_LIMIT
            next effectdata.ljust(EFFECT_LIMIT,?\0)
        end

        metadata = have_metadata ? songlist.map.with_index do |song,i|
            data = "".b
            song.rsx_metadata.each do |type,content|
                data << type
                data << content.encode(Encoding::ISO_8859_15).force_encoding(Encoding::ASCII_8BIT)
                data << ?\0
                data << ?\0 until (data.length % 4) == 0
            end
            raise "Song #{song.title.inspect} at index #{i} has too much metadata" if data.size > META_LIMIT
            next data.ljust(META_LIMIT,?\0)
        end : []

        songoffset = 1
        effectoffset = songoffset+(SONG_PAGES+COG_PAGES)*songs.size
        metaoffset = have_metadata ? effectoffset+EFFECT_PAGES*effects.size : 0        
        header = ["-RETROSNDBUNDLE-",    songs.size,effects.size,songoffset,effectoffset,metaoffset]
            .pack("a*                x10 C          C            L<         L<           L<         x472")
        raise unless header.size == 512
        return header + songs.join + effects.join + metadata.join
    end

    def self.compile_cogimage(code,homespun_path:DEFAULT_HOMESPUN_PATH)
        require 'tempfile'
        Dir::Tmpname.create("rsndasm") do |codefilepath|
            codefilepath << '.spin'
            begin
                File.write(codefilepath,code)
                Dir::Tmpname.create("rsndcog") do |binpath|
                    begin
                        cmdline = "#{"mono " unless Gem.win_platform?}#{homespun_path} #{codefilepath} -c -b -o #{binpath}"
                        puts cmdline
                        stdo = `#{cmdline}`
                        puts stdo
                        return File.binread(binpath+'.dat')
                    ensure
                        Dir.glob(binpath+'.*').each{|f|File.delete f}
                    end
                end
            ensure
                File.delete codefilepath if File.exists? codefilepath
            end
        end
    end

end

if __FILE__ == $0
    if ARGV.size < 5
        puts <<~USAGE
            RSX Compiler
            generate an "RSX" song bundle
            Usage:
            rsxcompiler <output type> <relocation address (hex)> <song binary>... <effect list> <effect binary> <output file>

            <output type> must be "stereo","dualmono" or "mono"

            You can also "require" this for use as a library
        USAGE
        exit
    end

    sfxlist = Retronitus.parse_sfxlist(File.read(ARGV[-3]))
    File.binwrite ARGV[-1],RSX_Compiler.compile_rsx(
        ARGV[0].to_sym,ARGV[1].to_i(16),sfxlist,Retronitus.parse_sfx_binary(File.binread(ARGV[-2])),
        *(ARGV[2..-4].map{|f|Retronitus.parse_binary(File.binread(f))})
    )
    puts "OK."
end
