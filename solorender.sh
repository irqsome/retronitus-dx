
SONGPATH=$1
PLAYLENGTH=$2

render(){
    ruby rsndplay.rb $SONGPATH $2 -t $PLAYLENGTH -w $1.raw
    ffmpeg -y -f s32le -ar 64k -ac 2 -i $1.raw $1.wav
}

render "out_all" "" &
for i in {0..7}
do
    render "out_ch$i" "-c $i" &
done
wait
