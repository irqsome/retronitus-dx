PUB melody
  return @MUSIC

OBJ
header : "retronitus_header"

DAT 
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                                     Music header
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
              byte "-RETROSNDTITLE-"
              byte "Alice in Wonderland",0
              byte "-RETROSNDCOMMENT-"
              byte "Cover of the extra stage theme from Touhou 5: Mystic Square",0
              byte "-RETROSNDAUTHOR-"
              byte "Wuerfel_21 (original by ZUN)",0
              byte "-RETROSNDCHANNELS-"
              byte long header#WAVE_SQUARE   | header#PAN_CENTER
              byte long header#WAVE_SQUARE   | header#PAN_CENTER
              byte long header#WAVE_SQUARE   | header#PAN_SURROUND
              byte long header#WAVE_SAWTOOTH | header#PAN_LEFT
              byte long header#WAVE_SAWTOOTH | header#PAN_RIGHT
              byte long header#WAVE_SAWTOOTH | header#PAN_SURROUND
              byte long header#WAVE_TRIANGLE | header#PAN_CENTER | ( 1 * header#AMPLIFY )
              byte long header#WAVE_LFSR | header#AMPLIFY * 1 ' sfx ready

              long 1
                                           
              byte "-RETROSNDHEAD-"
MUSIC         word  0 
'──────────────────────────────────────────────────────────────────────────────────────────
B
SEQ_INIT      word  @PATTSEQ_SQW1                  -@B  ' Square wave channel 1
              word  @PATTSEQ_BASS                  -@B  ' Square wave channel 2 
              word  @PATTSEQ_SQW2                  -@B  ' Square wave channel 3
              word  @PATTSEQ_SAW1                  -@B  ' Saw wave channel 1 (left)
              word  @PATTSEQ_SAW2                  -@B  ' Saw wave channel 2 (right)
              word  @PATTSEQ_SAW3                  -@B  ' Saw wave channel 3 (surround)
              word  @PATTSEQ_TRI                   -@B  ' Triangle channel
              word  @PATTSEQ_NOISE                 -@B  ' Noise channel                                  
'──────────────────────────────────────────────────────────────────────────────────────────        
INS_INIT      word @SILENT_INSTR -@B
              word @SQW_VIBRA -@B
              word @SQW_BASS  -@B
              word @SQW_BRASS1 -@B
              word @SQW_BRASS2 -@B
              word @SAW_HLIGHT -@B
              word @SAW_BLEEP -@B
              word @TRI_PREC_trg0 -@B
              word @SAW_ACCOMP -@B
              word  END 
                         
DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                               Pattern sequency data
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
  long 1

PATTSEQ_SQW1 ''Foreground
  word                                         (INSTR *0)|3, (@PATT_MEH -@B)<<1
  word (OCTAVE *6) | (NOTE *0)               | (INSTR *1)|3, (@PATT_INTROLEAD -@B)<<1
  word (OCTAVE *7) | (NOTE *2)               | (INSTR *1)|3, (@PATT_INTROLEADX -@B)<<1
  word (OCTAVE *6) | (NOTE *0)               | (INSTR *1)|3, (@PATT_INTROLEAD -@B)<<1
  word (OCTAVE *7) | (NOTE *2)               | (INSTR *3)|3, (@PATT_BRASSLEAD1 -@B)<<1
  word (OCTAVE *7) | (NOTE *2)               | (INSTR *3)|3, (@PATT_SILENCE_HALF -@B)<<1
  word (OCTAVE *7) | (NOTE *5)               | (INSTR *3)|3, (@PATT_BRASSLEAD2 -@B)<<1
  word (OCTAVE *7) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *7) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *7) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *7) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *5) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *5) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *5) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *5) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1END -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *1)|3, (@PATT_INTROLEAD -@B)<<1
  word (OCTAVE *7) | (NOTE *3)               | (INSTR *1)|3, (@PATT_INTROLEADX -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *1)|3, (@PATT_INTROLEAD -@B)<<1
  word (OCTAVE *7) | (NOTE *3)               | (INSTR *1)|3, (@PATT_INTROLEADX -@B)<<1
  word (OCTAVE *7) | (NOTE *1)               | (INSTR *3)|3, (@PATT_BRASSSTART2 -@B)<<1
  word (OCTAVE *7) | (NOTE *1)               | (INSTR *3)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *7) | (NOTE *1)               | (INSTR *3)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *3)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *3)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *3)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *3)|3, (@PATT_BRASSEND2 -@B)<<1
  word                                         (INSTR *0)|3, (@PATT_MEH -@B)<<1
PATTSEQ_SQW1_END
  word ((@PATTSEQ_SQW1_END-@PATTSEQ_SQW1)<<4)|13

PATTSEQ_SQW2  ''Foreground 
  word (OCTAVE *7) | (NOTE *5)               | (INSTR *0)|3, (@PATT_MEH -@B)<<1
  word (OCTAVE *7) | (NOTE *5)               | (INSTR *1)|3, (@PATT_INTROCHORDS1 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *7) | (NOTE *5)               | (INSTR *1)|3, (@PATT_INTROCHORDS2 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_INTROCHORDS1 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_INTROCHORDS3 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *7) | (NOTE *5)               | (INSTR *1)|3, (@PATT_INTROCHORDS1 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *7) | (NOTE *5)               | (INSTR *1)|3, (@PATT_INTROCHORDS2 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_INTROCHORDS1 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_INTROCHORDS3 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *4)|3, (@PATT_BRASSLEAD1 -@B)<<1
  word                                                       (@PATT_SILENCE_HALF -@B)<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4)|3, (@PATT_BRASSLEAD2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *1)|3, (@PATT_VIBVERSE1END -@B)<<1
  word (OCTAVE *7) | (NOTE *6)               | (INSTR *1)|3, (@PATT_INTROCHORDS1 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *7) | (NOTE *6)               | (INSTR *1)|3, (@PATT_INTROCHORDS2 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *1)|3, (@PATT_INTROCHORDS1 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *1)|3, (@PATT_INTROCHORDS3 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *7) | (NOTE *6)               | (INSTR *1)|3, (@PATT_INTROCHORDS1 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *7) | (NOTE *6)               | (INSTR *1)|3, (@PATT_INTROCHORDS2 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *1)|3, (@PATT_INTROCHORDS1 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *1)|3, (@PATT_INTROCHORDS3 -@B)<<1
  word                                                       (@PATT_SILENCE -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *4)|3, (@PATT_BRASSSTART2 -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *4)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *4)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *5) | (NOTE *1)               | (INSTR *4)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *5) | (NOTE *1)               | (INSTR *4)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *5) | (NOTE *1)               | (INSTR *4)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *5) | (NOTE *1)               | (INSTR *4)|3, (@PATT_BRASSEND2 -@B)<<1
  word                                         (INSTR *0)|3, (@PATT_MEH -@B)<<1
PATTSEQ_SQW2_END
  word ((@PATTSEQ_SQW2_END-@PATTSEQ_SQW2)<<4)|13

PATTSEQ_SAW3 '' Accomp stuff
  word (OCTAVE *7) | (NOTE *5)               | (INSTR *0)|3, (@PATT_MEH -@B)<<1
  word (OCTAVE *8) | (NOTE *0)               | (INSTR *8)|3, (@PATT_ACCOMP1 -@B)<<1
  word (OCTAVE *9) | (NOTE *8)               | (INSTR *8)|3, (@PATT_ACCOMP2 -@B)<<1
  word (OCTAVE *9) | (NOTE *8)               | (INSTR *8)|3, (@PATT_ACCOMP3 -@B)<<1
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *8)|3, (@PATT_ACCOMP5 -@B)<<1
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *8)|3, (@PATT_ACCOMP5 -@B)<<1
  
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *8)|3, (@PATT_ACCOMP5 -@B)<<1
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *8)|3, (@PATT_ACCOMP5 -@B)<<1
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *8)|3, (@PATT_ACCOMP5 -@B)<<1
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *8)|3, (@PATT_ACCOMP5 -@B)<<1
  
  word                                                       (@PATT_SILENCE_QUAD -@B)<<1[8]
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *8)|3, (@PATT_ACCOMP1 -@B)<<1
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *8)|3, (@PATT_ACCOMP2 -@B)<<1
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *8)|3, (@PATT_ACCOMP3 -@B)<<1
  word (OCTAVE *8) | (NOTE *6)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *8)|3, (@PATT_ACCOMP5 -@B)<<1
  word (OCTAVE *8) | (NOTE *6)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *6)               | (INSTR *8)|3, (@PATT_ACCOMP4 -@B)<<1
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *8)|3, (@PATT_ACCOMP5 -@B)<<1
  word (OCTAVE *7) | (NOTE *1)               | (INSTR *8)|3, (@PATT_BRASSSTART2 -@B)<<1
  word (OCTAVE *7) | (NOTE *1)               | (INSTR *8)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *7) | (NOTE *1)               | (INSTR *8)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *7) | (NOTE *1)               | (INSTR *8)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *7) | (NOTE *1)               | (INSTR *8)|3, (@PATT_VIBVERSE1X -@B)<<1
  word (OCTAVE *7) | (NOTE *1)               | (INSTR *8)|3, (@PATT_VIBVERSE1 -@B)<<1
  word (OCTAVE *7) | (NOTE *1)               | (INSTR *8)|3, (@PATT_BRASSEND2 -@B)<<1
  word                                         (INSTR *0)|3, (@PATT_MEH -@B)<<1
PATTSEQ_SAW3_END
  word ((@PATTSEQ_SAW3_END-@PATTSEQ_SAW3)<<4)|13



PATTSEQ_SAW1 '' Left highlight
  word (OCTAVE *7) | (NOTE *5)               | (INSTR *0)|3, (@PATT_MEH -@B)<<1
  word                                                       (@PATT_SILENCE_DOUBLE -@B)<<1[2]
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTA -@B)<<1
  word                                                       (@PATT_SILENCE_DOUBLE -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTA -@B)<<1
  word                                                       (@PATT_SILENCE_DOUBLE -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTA -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *6)|3, (@PATT_BLEEP3 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTA -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *6)|3, (@PATT_BLEEP3 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTA -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *6)|3, (@PATT_BLEEP3 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTA -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *6)|3, (@PATT_BLEEP3 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTA -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *6)|3, (@PATT_BLEEP3 -@B)<<1
  
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *0)|3, (@PATT_LONGMEH -@B)<<1 ''Force note-off!!!
  word                                                       (@PATT_SILENCE_QUAD -@B)<<1[3]
  word                                                       (@PATT_SILENCE_DOUBLE -@B)<<1
  word (OCTAVE *6) | (NOTE *0)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *8)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *10)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *0)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *0)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *8)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *10)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *0)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word                                                       (@PATT_SILENCE_QUAD -@B)<<1
  word (OCTAVE *6) | (NOTE *4)               | (INSTR *5)|3, (@PATT_HIGHLIGHTA -@B)<<1
  word                                                       (@PATT_SILENCE_DOUBLE -@B)<<1
  word (OCTAVE *6) | (NOTE *4)               | (INSTR *5)|3, (@PATT_HIGHLIGHTA -@B)<<1
  word                                                       (@PATT_SILENCE_DOUBLE -@B)<<1
  word (OCTAVE *6) | (NOTE *4)               | (INSTR *5)|3, (@PATT_HIGHLIGHTA -@B)<<1
  word (OCTAVE *5) | (NOTE *1)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *4)               | (INSTR *6)|3, (@PATT_BLEEP3 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *0)|3, (@PATT_LONGMEH -@B)<<1 ''Force note-off!!!
  word                                                       (@PATT_SILENCE_QUAD -@B)<<1[3]
  word                                                       (@PATT_SILENCE_DOUBLE -@B)<<1
  word                                                       (@PATT_SILENCE_HALF -@B)<<1
  word (OCTAVE *7) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *7)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *9)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *10)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *10)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *7)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *9)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *10)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1 
  word                                          (@PATT_SILENCE -@B)<<1
PATTSEQ_SAW1_END
  word ((@PATTSEQ_SAW1_END-@PATTSEQ_SAW1)<<4)|13

PATTSEQ_SAW2 '' Right highlight
  word (OCTAVE *7) | (NOTE *5)               | (INSTR *0)|3, (@PATT_MEH -@B)<<1
  word                                                       (@PATT_SILENCE_QUAD -@B)<<1
  word                                                       (@PATT_SILENCE_DOUBLE -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTB -@B)<<1
  word                                                       (@PATT_SILENCE_DOUBLE -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTB -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *6)|3, (@PATT_BLEEP2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTB -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *6)|3, (@PATT_BLEEP2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTB -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *6)|3, (@PATT_BLEEP2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTB -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *6)|3, (@PATT_BLEEP2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTB -@B)<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *6)|3, (@PATT_BLEEP2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHTB -@B)<<1
  
  word (OCTAVE *7) | (NOTE *5)               | (INSTR *0)|3, (@PATT_MEH_SILENCE_QUAD -@B)<<1
  word                                                       (@PATT_SILENCE_TRIPLE -@B)<<1
  word                                                       (@PATT_SILENCE_QUAD -@B)<<1[2]
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *0)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *7)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *5) | (NOTE *5)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *0)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *7)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *5) | (NOTE *5)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word                                                       (@PATT_SILENCE_QUAD -@B)<<1
  word                                                       (@PATT_SILENCE_DOUBLE -@B)<<1
  word (OCTAVE *6) | (NOTE *4)               | (INSTR *5)|3, (@PATT_HIGHLIGHTB -@B)<<1
  word                                                       (@PATT_SILENCE_DOUBLE -@B)<<1
  word (OCTAVE *6) | (NOTE *4)               | (INSTR *5)|3, (@PATT_HIGHLIGHTB -@B)<<1
  word (OCTAVE *5) | (NOTE *1)               | (INSTR *6)|3, (@PATT_BLEEP1 -@B)<<1
  word (OCTAVE *6) | (NOTE *4)               | (INSTR *6)|3, (@PATT_BLEEP2 -@B)<<1
  word (OCTAVE *6) | (NOTE *4)               | (INSTR *5)|3, (@PATT_HIGHLIGHTB -@B)<<1
  word (OCTAVE *7) | (NOTE *5)               | (INSTR *0)|3, (@PATT_MEH_SILENCE_QUAD -@B)<<1
  word                                                       (@PATT_SILENCE_TRIPLE -@B)<<1
  word                                                       (@PATT_SILENCE_QUAD -@B)<<1[2]
  word                                                       (@PATT_SILENCE_HALF -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *6)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *5) | (NOTE *4)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *7) | (NOTE *11)              | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *6)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *5) | (NOTE *4)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *1)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1
  word (OCTAVE *6) | (NOTE *2)               | (INSTR *5)|3, (@PATT_HIGHLIGHT2 -@B)<<1 
  word                                          (@PATT_SILENCE -@B)<<1
PATTSEQ_SAW2_END
  word ((@PATTSEQ_SAW2_END-@PATTSEQ_SAW2)<<4)|13


PATTSEQ_BASS
  word                                         (INSTR *0)|3, (@PATT_MEH -@B)<<1
  word                                                       (@PATT_SILENCE_QUAD -@B)<<1[2]
  
  word (OCTAVE *8) | (NOTE *0)               | (INSTR *2)|3, (@PATT_SLOWBASS1 -@B)<<1[2]
  word (OCTAVE *9) | (NOTE *8)               | (INSTR *2)|3, (@PATT_SLOWBASS2 -@B)<<1[2]
  
  word (OCTAVE *8) | (NOTE *0)               | (INSTR *2)|3, (@PATT_FASTBASS1_4 -@B)<<1
  word (OCTAVE *9) | (NOTE *8)               | (INSTR *2)|3, (@PATT_FASTBASS2 -@B)<<1[4]
  word (OCTAVE *8) | (NOTE *0)               | (INSTR *2)|3, (@PATT_FASTBASS1_4 -@B)<<1
  word (OCTAVE *9) | (NOTE *8)               | (INSTR *2)|3, (@PATT_FASTBASS2 -@B)<<1[4]
  word (OCTAVE *8) | (NOTE *0)               | (INSTR *2)|3, (@PATT_FASTBASS1_4 -@B)<<1
  word (OCTAVE *9) | (NOTE *8)               | (INSTR *2)|3, (@PATT_FASTBASS2 -@B)<<1[4]
  word (OCTAVE *8) | (NOTE *0)               | (INSTR *2)|3, (@PATT_FASTBASS1_4 -@B)<<1
  word (OCTAVE *9) | (NOTE *8)               | (INSTR *2)|3, (@PATT_FASTBASS2 -@B)<<1[4]
  word (OCTAVE *8) | (NOTE *0)               | (INSTR *2)|3, (@PATT_FASTBASS1_4 -@B)<<1
  word (OCTAVE *9) | (NOTE *8)               | (INSTR *2)|3, (@PATT_FASTBASS2 -@B)<<1[4]
  word (OCTAVE *8) | (NOTE *7)               | (INSTR *2)|3, (@PATT_MEH -@B)<<1 '34
  word (OCTAVE *8) | (NOTE *3)               | (INSTR *2)|3, (@PATT_MEH -@B)<<1 '35
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_MEH -@B)<<1 '36
  word (OCTAVE *8) | (NOTE *7)               | (INSTR *2)|3, (@PATT_MEH -@B)<<1 '37
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *2)|3, (@PATT_MEH -@B)<<1 '38
  word (OCTAVE *7) | (NOTE *0)               | (INSTR *2)|3, (@PATT_MEH -@B)<<1 '39
  word (OCTAVE *8) | (NOTE *11)              | (INSTR *2)|3, (@PATT_MEH -@B)<<1 '40
  word (OCTAVE *8) | (NOTE *11)              | (INSTR *2)|3, (@PATT_MEHMEH -@B)<<1 '41
  word (OCTAVE *8) | (NOTE *0)               | (INSTR *2)|3, (@PATT_SLOWBASS3 -@B)<<1 '42
  word (OCTAVE *8) | (NOTE *0)               | (INSTR *2)|3, (@PATT_SLOWBASS3 -@B)<<1 '43
  word (OCTAVE *9) | (NOTE *10)              | (INSTR *2)|3, (@PATT_SLOWBASS3 -@B)<<1 '44
  word (OCTAVE *8) | (NOTE *3)               | (INSTR *2)|3, (@PATT_SLOWBASS4 -@B)<<1 '45
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_SLOWBASS5 -@B)<<1 '46
  word (OCTAVE *8) | (NOTE *3)               | (INSTR *2)|3, (@PATT_SLOWBASS4 -@B)<<1 '47
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_SLOWBASS4 -@B)<<1'48
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *2)|3, (@PATT_FASTBASS3 -@B)<<1'49
  word (OCTAVE *8) | (NOTE *0)               | (INSTR *2)|3, (@PATT_FASTBASS1_4 -@B)<<1'50
  word (OCTAVE *9) | (NOTE *10)              | (INSTR *2)|3, (@PATT_FASTBASS2 -@B)<<1[2]'52
  word (OCTAVE *8) | (NOTE *3)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2]'53
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS5 -@B)<<1[2]'54
  word (OCTAVE *8) | (NOTE *3)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2]'55
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS6 -@B)<<1[2]'56
  word (OCTAVE *8) | (NOTE *5)               | (INSTR *2)|3, (@PATT_FASTBASS7 -@B)<<1'57
  word (OCTAVE *8) | (NOTE *0)               | (INSTR *2)|3, (@PATT_FASTBASS1_4 -@B)<<1 '58
  word (OCTAVE *9) | (NOTE *10)              | (INSTR *2)|3, (@PATT_FASTBASS2 -@B)<<1[2] '60
  word (OCTAVE *8) | (NOTE *3)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2] '61
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS5 -@B)<<1[2] '62
  word (OCTAVE *8) | (NOTE *3)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2] '63
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS6 -@B)<<1[2] '64
  word (OCTAVE *8) | (NOTE *3)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2] '65
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_MEH -@B)<<1[2]
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *2)|3, (@PATT_MEH -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_MEH -@B)<<1[2]
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *2)|3, (@PATT_MEH -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_SLOWBASS1 -@B)<<1
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_SLOWBASS1 -@B)<<1
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *2)|3, (@PATT_SLOWBASS6 -@B)<<1
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *2)|3, (@PATT_SLOWBASS6 -@B)<<1
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_SLOWBASS1 -@B)<<1
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_SLOWBASS1 -@B)<<1
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *2)|3, (@PATT_SLOWBASS2 -@B)<<1
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *2)|3, (@PATT_SLOWBASS2 -@B)<<1
  word (OCTAVE *7) | (NOTE *0)               | (INSTR *2)|3, (@PATT_BENDBASS -@B)<<1 ''82
  word (OCTAVE *9) | (NOTE *11)              | (INSTR *2)|3, (@PATT_FASTBASS1_4 -@B)<<1
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *2)|3, (@PATT_FASTBASS2 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_FASTBASS5 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_FASTBASS6 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *4)               | (INSTR *2)|3, (@PATT_FASTBASS7 -@B)<<1
  word (OCTAVE *9) | (NOTE *11)              | (INSTR *2)|3, (@PATT_FASTBASS1_4 -@B)<<1
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *2)|3, (@PATT_FASTBASS2 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_FASTBASS5 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_FASTBASS6 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *4)               | (INSTR *2)|3, (@PATT_FASTBASS7 -@B)<<1
  word (OCTAVE *9) | (NOTE *11)              | (INSTR *2)|3, (@PATT_FASTBASS1_4 -@B)<<1
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *2)|3, (@PATT_FASTBASS2 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_FASTBASS5 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_FASTBASS6 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *4)               | (INSTR *2)|3, (@PATT_FASTBASS7 -@B)<<1
  word (OCTAVE *9) | (NOTE *11)              | (INSTR *2)|3, (@PATT_FASTBASS1_4 -@B)<<1
  word (OCTAVE *9) | (NOTE *9)               | (INSTR *2)|3, (@PATT_FASTBASS2 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_FASTBASS5 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *1)               | (INSTR *2)|3, (@PATT_FASTBASS6 -@B)<<1[2]
  word (OCTAVE *8) | (NOTE *2)               | (INSTR *2)|3, (@PATT_FASTBASS4 -@B)<<1[2] 
  word                                          (@PATT_SILENCE -@B)<<1
  
PATTSEQ_BASS_END
  word ((@PATTSEQ_BASS_END-@PATTSEQ_BASS)<<4)|13

PATTSEQ_NOISE
  'long SET_TEMPO|($FFF00000) | (INSTR *0)|3, (@PATT_SILENCE -@B)<<1
  'long SET_TEMPO|(((BPM*150)/8)&$FFF00000) | (INSTR *0)|3, (@PATT_SILENCE -@B)<<1
  'word ($7FF0)|1
  'word (@PATT_SILENCE_QUAD -@B)<<1[28]
  word ((((BPM*150)/8)>>15)&$FFF0)|1
  word 13

PATTSEQ_TRI
  word                                         (INSTR *0)|3, (@PATT_MEH -@B)<<1
  word                                         (INSTR *7)|3,(@PATT_SILENCE_QUAD -@B)<<1
  word                                          (@PATT_SILENCE_DOUBLE -@B)<<1
  word                                          (@PATT_SILENCE -@B)<<1
  word                                          (@PATT_DRUMSTART -@B)<<1
  word                                          (@PATT_RYTHM1 -@B)<<1[3]
  word                                          (@PATT_RYTHM2 -@B)<<1
  word                                          (@PATT_RYTHM1 -@B)<<1[3]
  word                                          (@PATT_RYTHM3 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM6 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM7 -@B)<<1
  word                                          (@PATT_RYTHM8 -@B)<<1[3]
  word                                          (@PATT_RYTHM9 -@B)<<1
  word                                          (@PATT_RYTHM1 -@B)<<1[2]
  word                                          (@PATT_RYTHM2 -@B)<<1
  word                                          (@PATT_RYTHM10 -@B)<<1
  word                                          (@PATT_RYTHM11 -@B)<<1
  word                                          (@PATT_RYTHM12 -@B)<<1
  word                                          (@PATT_RYTHM11 -@B)<<1[3]
  word                                          (@PATT_RYTHM7 -@B)<<1
  word                                          (@PATT_RYTHM4C5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM13 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM7 -@B)<<1
  word                                          (@PATT_RYTHM14 -@B)<<1
  word                                          (@PATT_SILENCE_DOUBLE -@B)<<1
  word                                          (@PATT_SILENCE_QUAD -@B)<<1
  word                                          (@PATT_DRUMSTART -@B)<<1
  word                                          (@PATT_RYTHM15 -@B)<<1[4]
  word                                          (@PATT_RYTHM1 -@B)<<1[3]
  word                                          (@PATT_RYTHM7 -@B)<<1
  word                                          (@PATT_RYTHM16 -@B)<<1
  word                                          (@PATT_RYTHM8 -@B)<<1[3]
  word                                          (@PATT_RYTHM17 -@B)<<1
  word                                          (@PATT_RYTHM1 -@B)<<1[3]
  word                                          (@PATT_RYTHM2 -@B)<<1
  word                                          (@PATT_RYTHM10 -@B)<<1
  word                                          (@PATT_RYTHM11 -@B)<<1
  word                                          (@PATT_RYTHM12 -@B)<<1
  word                                          (@PATT_RYTHM11 -@B)<<1[3]
  word                                          (@PATT_RYTHM7 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM6 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM5 -@B)<<1
  word                                          (@PATT_RYTHM4 -@B)<<1
  word                                          (@PATT_RYTHM7 -@B)<<1
  word                                          (@PATT_DRUMEND -@B)<<1 
  word                                          (@PATT_SILENCE_HALF -@B)<<1
  'long END
PATTSEQ_TRI_END
  word ((@PATTSEQ_TRI_END-@PATTSEQ_TRI)<<4)|13
  word $fffe
  




DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'                                  Pattern data
'──────────────────────────────────────────────────────────────────────────────────────────


PATT_BRASSEND2          byte ((12 + 5) << 3) | 5
                        byte ((12 +10) << 3) | 5
                        byte ((12 - 3) << 3) | 3
                        byte ((12 + 1) << 3) | 7
                        byte ___             | 7
                        
                        byte ___             | 7
                        byte ___             | 7
                        byte END



PATT_BRASSSTART2        byte ___             | 7
                        byte ((12    ) << 3) | 1
                        byte ((12 + 1) << 3) | 1
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 2) << 3) | 7
                        byte ___             | 7
                        
                        byte ((12 - 7) << 3) | 5
                        byte ((12 + 3) << 3) | 5
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 5) << 3) | 7
                        byte ___             | 7
                                   
                        byte ((12 -12) << 3) | 5
                        byte ((12 + 3) << 3) | 4
                        byte ((12 + 7) << 3) | 0
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 2) << 3) | 7
                        byte ___             | 7
                        
                        byte ((12 - 5) << 3) | 5
                        byte ((12 - 3) << 3) | 5
                        byte ((12 + 8) << 3) | 2
                        byte ((12 - 3) << 3) | 0
                        byte ((12 + 1) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 1) << 3) | 1
                        byte ((12 - 2) << 3) | 1
                        
                        byte ((12 + 2) << 3) | 7
                        byte ___             | 7
                        byte ((12    ) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 1) << 3) | 3
                        
                        byte ((12 + 1) << 3) | 7
                        byte ((12 + 7) << 3) | 7
                        byte ((12 - 7) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 1) << 3) | 3
                        
                        byte ((12 + 1) << 3) | 6
                        byte ((12 + 7) << 3) | 0
                        byte ((12 + 2) << 3) | 7
                        byte ((12 - 1) << 3) | 5
                        byte ((12 + 1) << 3) | 5
                        byte ((12 + 2) << 3) | 3
                        
                        byte ((12 - 7) << 3) | 7
                        byte ___             | 7
                        byte ((12 + 4) << 3) | 7 
                        byte ___             | 7
                        
                        

                        byte END
                        



PATT_VIBVERSE1          byte ((12    ) << 3) | 5
                        byte ((12 + 1) << 3) | 5
                        byte ((12 + 2) << 3) | 3

                        
                        byte ((12 + 2) << 3) | 7
                        byte ___             | 7

                        byte ((12 - 7) << 3) | 5
                        byte ((12 + 3) << 3) | 5
                        byte ((12 + 4) << 3) | 3

                        
                        byte ((12 + 5) << 3) | 7
                        byte ___             | 7
                        byte ((12 -12) << 3) | 5
                        byte ((12 + 3) << 3) | 4
                        byte ((12 + 7) << 3) | 0
                        byte ((12 + 2) << 3) | 3

                        byte ((12 - 2) << 3) | 7
                        byte ___             | 7
                        byte ((12 - 5) << 3) | 5
                        byte ((12 - 3) << 3) | 5
                        byte ((12 + 8) << 3) | 2
                        byte ((12 - 3) << 3) | 0

                        byte ((12 + 1) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 1) << 3) | 1
                        byte ((12 - 2) << 3) | 1
                        byte ((12 + 2) << 3) | 7
                        byte ___             | 7

                        byte ((12    ) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 1) << 3) | 3
                        byte ((12 + 1) << 3) | 7
                        byte ((12 + 7) << 3) | 7
                        
                        byte ((12 - 7) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 1) << 3) | 3
                        byte ((12 + 1) << 3) | 6
                        byte ((12 + 7) << 3) | 0
                        byte ((12 + 2) << 3) | 7

                        byte ((12 - 1) << 3) | 5
                        byte ((12 + 1) << 3) | 5
                        byte ((12 + 2) << 3) | 3
                        byte END

PATT_VIBVERSE1X         byte ((12 + 5) << 3) | 7
                        byte ___             | 7
                                                
                        byte ((12 + 4) << 3) | 7
                        byte ___             | 7
                        byte END

PATT_VIBVERSE1END       byte ((12 + 5) << 3) | 5
                        byte ((12 +10) << 3) | 5
                        byte ((12 - 3) << 3) | 3

                        byte ((12 + 1) << 3) | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte END
                        

                        



PATT_ACCOMP1            byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        
                        byte ((12    ) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte ___             | 7
                        byte ___             | 7
                        byte END ''giant delta!
PATT_ACCOMP2            byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        
                        byte ((12    ) << 3) | 3
                        byte ((12 + 6) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte ___             | 7
                        byte ((12 - 6) << 3) | 7
                        
                        byte ((12 - 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte ((12 - 5) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                            
                        byte ((12 - 2) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 5) << 3) | 7
                        byte ((12 - 5) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 7) << 3) | 3
                        
                        
                        byte ((12 -11) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte END '' What's with this channel and huge deltas?
                        
PATT_ACCOMP3            byte ((12    ) << 3) | 3
                        byte ((12 + 6) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 5) << 3) | 7
                        byte ((12 - 8) << 3) | 3
                        byte ((12 +10) << 3) | 3
                        byte ((12 - 5) << 3) | 3
                        ''FALL-THROUGH!
PATT_ACCOMP4            byte ((12 - 5) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 7) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte END

PATT_ACCOMP5            byte ((12 - 5) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 7) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        ''FALL-THROUGH!
PATT_ACCOMP6            byte ((12 -12) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 7) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte END
                        





PATT_DRUMSTART          byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte TRG_0           | 1
                        byte TRG_0           | 1
                        byte TRG_3           | 3
                        byte END

PATT_RYTHM1             byte TRG_0           | 3 'Kick n' shake
                        byte TRG_1           | 3
                        byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte END


PATT_RYTHM2             byte TRG_0           | 3 'Kick n' shake + intense snare stuff
                        byte TRG_1           | 3
                        byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte END

PATT_RYTHM3             byte TRG_3           | 1 'Just snare!
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte END
                        
PATT_RYTHM4             byte TRG_0           | 3
                        byte TRG_3           | 3
                        byte TRG_0           | 3
                        byte TRG_3           | 1
                        byte TRG_0           | 1
                        byte TRG_0           | 3
                        byte TRG_3           | 3
                        byte TRG_0           | 3
                        byte TRG_3           | 3
                        byte END

PATT_RYTHM4C5           byte TRG_2           | 3
                        byte TRG_3           | 3
                        byte TRG_0           | 3
                        byte TRG_3           | 1
                        byte TRG_0           | 1
                        byte TRG_0           | 3
                        byte TRG_3           | 3
                        byte TRG_0           | 3
                        byte TRG_3           | 3
                        '' FALL-THROUGH!
PATT_RYTHM5             byte TRG_0           | 3
                        byte TRG_3           | 3
                        byte TRG_0           | 3
                        byte TRG_3           | 1
                        byte TRG_0           | 1
                        byte TRG_0           | 3
                        byte TRG_3           | 1
                        byte TRG_0           | 1
                        byte TRG_0           | 3
                        byte TRG_3           | 1
                        byte TRG_0           | 1
                        byte END

PATT_RYTHM6             byte TRG_0           | 3
                        byte TRG_3           | 3
                        byte TRG_0           | 3
                        byte TRG_3           | 1
                        byte TRG_0           | 1
                        
                        byte TRG_3           | 1
                        byte TRG_0           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_0           | 1
                        byte TRG_3           | 1
                        byte TRG_0           | 1
                        byte TRG_3           | 1
                        byte END

PATT_RYTHM7             byte TRG_3           | 1 'And you thought RYTHM3 was bad
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte TRG_3           | 0
                        byte END

PATT_RYTHM8             byte TRG_2           | 7 'Cymbal and Snare
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 3
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte END

PATT_RYTHM9             byte TRG_2           | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte TRG_0           | 1
                        byte TRG_0           | 1
                        byte TRG_2           | 1
                        byte TRG_0           | 1

                        byte TRG_2           | 7
                        byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte END

PATT_RYTHM10            byte TRG_2           | 7
                        byte TRG_3           | 7
                        byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte TRG_3           | 7
                        ''FALL_THROUGH
PATT_RYTHM11            byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte TRG_3           | 7
                        byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte TRG_3           | 7
                        byte END

PATT_RYTHM12            byte TRG_0           | 3
                        byte TRG_1           | 3
                        byte TRG_3           | 7
                        byte TRG_0           | 3
                        byte TRG_3           | 3
                        byte TRG_3           | 3
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte END

PATT_RYTHM13            byte TRG_0           | 3   'Like RYTHM5, but wiered
                        byte TRG_3           | 3
                        byte TRG_0           | 3
                        byte TRG_3           | 1
                        byte TRG_0           | 1
                        
                        byte TRG_0           | 3
                        byte TRG_3           | 1
                        byte TRG_0           | 1
                        byte TRG_0           | 3
                        byte TRG_3           | 1
                        byte TRG_3           | 1
                        byte END

PATT_RYTHM14            byte TRG_2           | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte END

PATT_RYTHM15            byte TRG_0           | 7 'Kick only
                        byte TRG_0           | 7
                        byte TRG_0           | 7
                        byte TRG_0           | 7
                        byte END

PATT_RYTHM16            byte TRG_2           | 7 'Cymbal, half a measure
                        byte ___             | 7
                        byte END

PATT_RYTHM17            byte TRG_2           | 7
                        byte ___             | 7
                        byte TRG_2           | 7
                        byte TRG_0           | 1 
                        byte TRG_0           | 1
                        byte TRG_2           | 1 
                        byte TRG_0           | 1
                        byte END

PATT_DRUMEND            byte TRG_2           | 7
                        byte TRG_0           | 1
                        byte TRG_0           | 1
                        byte TRG_3           | 3
                        byte END
                        


PATT_BLEEP1             byte ((12    ) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte ((12 + 9) << 3) | 1
                        byte ((12 -12) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte ((12 - 8) << 3) | 1   
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 2) << 3) | 1 
                        byte ((12 + 1) << 3) | 1
                        byte ((12 - 8) << 3) | 1
                        byte ((12 + 8) << 3) | 1
                        byte ((12 - 8) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12 - 9) << 3) | 1
                        byte END

PATT_BLEEP2             byte ((12    ) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12    ) << 3) | 1 
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12    ) << 3) | 1 
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12    ) << 3) | 1  
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte END

PATT_BLEEP3             byte ((12    ) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12    ) << 3) | 1 
                        byte ((12 -12) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12    ) << 3) | 1 
                        byte ((12 -12) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 -10) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte END




PATT_HIGHLIGHTA         byte TRG_1           | 7 ''Might need some optimization
                        byte ___             | 7
                        byte ((12    ) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 5) << 3) | 3
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 5) << 3) | 3
                        
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 5) << 3) | 3
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 5) << 3) | 3
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 5) << 3) | 3
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 5) << 3) | 3
                        byte END


PATT_HIGHLIGHTB         byte TRG_1           | 7 ''Might need some optimization
                        byte ___             | 7
                        byte ((12    ) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 4) << 3) | 3
                        
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 9) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 4) << 3) | 3
                        byte END


PATT_HIGHLIGHT2         byte TRG_1           | 3
                        byte ((12    ) << 3) | 3
                        byte TRG_1           | 3
                        byte ((12    ) << 3) | 3
                        byte TRG_1           | 3
                        byte ((12    ) << 3) | 3
                        byte TRG_1           | 3
                        byte ((12    ) << 3) | 3
                        byte END



PATT_BRASSLEAD1         byte ___             | 7
                        byte ___             | 7
                        byte ((12    ) << 3) | 5
                        byte ((12 + 1) << 3) | 5
                        byte ((12 + 2) << 3) | 3

PATT_BRASSLEAD2         byte ((12 + 7) << 3) | 0
                        byte ((12 + 2) << 3) | 6 
                        byte ___             | 3
                        byte ((12 - 2) << 3) | 7
                        byte ___             | 7
                        byte ___             | 3

                        byte ((12 - 7) << 3) | 0
                        byte ((12 + 2) << 3) | 4
                        byte ((12 - 2) << 3) | 5
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 7) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 + 1) << 3) | 3

                        byte ((12 + 9) << 3) | 0
                        byte ((12 + 2) << 3) | 6 
                        byte ___             | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 - 3) << 3) | 7
                        byte ___             | 7
                        
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7

                        
                        byte ((12 + 2) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 2) << 3) | 3
                        byte ___             | 7
                        byte ___             | 7

                        
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7

                        byte ((12 + 2) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 - 3) << 3) | 7
                        byte ___             | 7

                        byte ___             | 7
                        byte ___             | 7
 
                        byte END

                        
                        


PATT_INTROLEAD
                        
                         byte ((12    ) << 3) | 0
                         byte ((12 + 2) << 3) | 2
                         byte ___             | 7
                         byte ((12 - 2) << 3) | 3
                         byte ___             | 7
                         byte ___             | 7
                         

                         
                         byte ((12 - 7) << 3) | 0
                         byte ((12 + 2) << 3) | 4
                         byte ((12 - 2) << 3) | 5
                         byte ((12 + 2) << 3) | 3
                         byte ((12 - 7) << 3) | 5
                         byte ((12 + 2) << 3) | 5
                         byte ((12 + 1) << 3) | 3

                         
                         byte ((12 + 9) << 3) | 0
                         byte ((12 + 2) << 3) | 2
                         byte ___             | 7
                         byte ((12 + 1) << 3) | 3
                         byte ((12 - 3) << 3) | 7
                         byte ___             | 7
                         
                         byte ___             | 7
                         byte ___             | 7
                         byte ((12 - 0) << 3) | 0
                         byte ((12 + 2) << 3) | 4
                         byte ((12 + 1) << 3) | 5
                         byte ((12 + 2) << 3) | 3

                         byte ((12 - 2) << 3) | 0
                         byte ((12 + 2) << 3) | 2
                         byte ___             | 7
                         byte ((12 - 2) << 3) | 3
                         byte ___             | 7
                         byte ___             | 7

                         byte ((12 + 2) << 3) | 0
                         byte ((12 + 2) << 3) | 4
                         byte ((12 - 2) << 3) | 5
                         byte ((12 + 2) << 3) | 3
                         byte ((12 - 7) << 3) | 5
                         byte ((12 + 2) << 3) | 5
                         byte ((12 + 1) << 3) | 3

                         byte ((12 + 0) << 3) | 0
                         byte ((12 + 2) << 3) | 2
                         byte ___             | 7
                         byte ((12 + 2) << 3) | 3
                         byte ((12 - 4) << 3) | 7
                         byte ___             | 7


                         '' end because giant delta
                         byte END

PATT_INTROLEADX         byte ___             | 7                                                  
                        byte ___             | 7
                        byte ((12 + 0) << 3) | 5 ''Annoying little bit at the end!
                        byte ((12 + 1) << 3) | 5
                        byte ((12 + 2) << 3) | 3
                        byte END
                         


PATT_INTROCHORDS1       byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 2) << 3) | 7
                        byte ___             | 7
                        byte ___             | 3  
                        byte END

PATT_INTROCHORDS2       byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 4) << 3) | 7
                        byte ___             | 7        
                        byte END

PATT_INTROCHORDS3       byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 - 3) << 3) | 7
                        byte ___             | 7
                        byte END

PATT_SLOWBASS1          byte ((12    ) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 7) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte END

PATT_SLOWBASS2          byte ((12    ) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 7) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte END


PATT_FASTBASS1_4        byte ((12    ) << 3) | 1 
                        byte ((12    ) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        
PATT_FASTBASS1_3        byte ((12    ) << 3) | 1  
                        byte ((12    ) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        
PATT_FASTBASS1_2        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 - 7) << 3) | 1

PATT_FASTBASS1          byte ((12    ) << 3) | 1  ''NOTE: only half a measure long!
                        byte ((12    ) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        byte END

PATT_FASTBASS2          byte ((12    ) << 3) | 1  ''NOTE: only half a measure long!
                        byte ((12    ) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        byte END

PATT_SLOWBASS3          byte ((12    ) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 7) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 7) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 7) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte END

PATT_SLOWBASS4          byte ((12    ) << 3) | 3
                        byte ((12 + 9) << 3) | 3
                        byte ((12 - 9) << 3) | 3
                        byte ((12 + 9) << 3) | 3
                        byte ((12 - 9) << 3) | 3
                        byte ((12 + 9) << 3) | 3
                        byte ((12 - 9) << 3) | 3
                        byte ((12 + 9) << 3) | 3
                        byte END

PATT_SLOWBASS5          byte ((12    ) << 3) | 3
                        byte ((12 +10) << 3) | 3
                        byte ((12 -10) << 3) | 3
                        byte ((12 +10) << 3) | 3
                        byte ((12 -10) << 3) | 3
                        byte ((12 +10) << 3) | 3
                        byte ((12 -10) << 3) | 3
                        byte ((12 +10) << 3) | 3
                        byte END

PATT_FASTBASS3          byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 6) << 3) | 1
                        byte ((12 - 6) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 6) << 3) | 1
                        byte ((12 - 6) << 3) | 1
                        byte ((12 + 2) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 6) << 3) | 1
                        byte ((12 - 6) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 6) << 3) | 1
                        byte ((12 - 6) << 3) | 1
                        byte END

PATT_FASTBASS4          byte ((12    ) << 3) | 1  ''NOTE: only half a measure long!
                        byte ((12    ) << 3) | 1
                        byte ((12 + 9) << 3) | 1
                        byte ((12 - 9) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 - 9) << 3) | 1
                        byte END

PATT_FASTBASS5          byte ((12    ) << 3) | 1  ''NOTE: only half a measure long!
                        byte ((12    ) << 3) | 1
                        byte ((12 +10) << 3) | 1
                        byte ((12 -10) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12 -10) << 3) | 1
                        byte END

PATT_FASTBASS6          byte ((12    ) << 3) | 1  ''NOTE: only half a measure long!
                        byte ((12    ) << 3) | 1
                        byte ((12 + 9) << 3) | 1
                        byte ((12 - 9) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 - 9) << 3) | 1
                        byte END

PATT_FASTBASS7          byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 6) << 3) | 1
                        byte ((12 - 6) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 6) << 3) | 1
                        byte ((12 - 6) << 3) | 1
                        byte ((12 + 2) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 - 4) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 - 4) << 3) | 1
                        byte END

PATT_SLOWBASS6          byte ((12    ) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 7) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte END

PATT_BENDBASS           byte ___             | 7
                        byte ((12    ) << 3) | 0
                        byte TRG_1           | 6
                        byte END

PATT_MEH_SILENCE_QUAD   byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
PATT_SILENCE_QUAD       byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
PATT_SILENCE_TRIPLE     byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
PATT_SILENCE_DOUBLE     byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
PATT_SILENCE            byte ___             | 7
                        byte ___             | 7
PATT_SILENCE_HALF       byte ___             | 7
                        byte ___             | 7

                        byte END

PATT_MEH                byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7 
                        byte END

PATT_LONGMEH            byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte END

PATT_MEHMEH             byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        '' end because long gap
                        byte END


DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                                    Instrument data
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────          
         long 1

SILENT_INSTR
    long    SET|VOLUME,                     $0000000
    long    JUMP,                           -1 *STEPS


SQW_VIBRA   'Piano? Guitar? Vibraphone? What is this even?
    long    SET|MODULATION,                 $00000000|$1AA
    long    SET|MODULATION,                 $30000000|$000
    long    SET|VOLUME,                     $FF000000
    long    SET|ENVELOPE,                   $0010CC00|$098
    long    MODIFY|FREQUENCY|(10 *wait_ms), $FFFFFE00
    long    SET|MODULATION,                 $00000000|$0B0
    long    MODIFY|FREQUENCY|(20 *wait_ms), $FFFFFE00
    long    SET|ENVELOPE,                   $FFFC5800|$000
    long    MODIFY|FREQUENCY|(10 *wait_ms), $FFFFFE00
    long    WAIT|(40 *wait_ms),             0
    long    SET|ENVELOPE,                   $FFFEF200|$000
    long    MODIFY|FREQUENCY|(80 *wait_ms), $00000400
    long    SET|ENVELOPE,                   $FFFF5A00|$000
    long    MODIFY|FREQUENCY|(80 *wait_ms), $FFFFFC00
    long    SET|ENVELOPE,                   $FFFF7000|$000
    long    MODIFY|FREQUENCY|(80 *wait_ms), $00000400
    long    SET|ENVELOPE,                   $FFFF8800|$000
    long    MODIFY|FREQUENCY|(80 *wait_ms), $FFFFFC00
    long    SET|ENVELOPE,                   $FFFF9400|$001
    long    MODIFY|FREQUENCY|(80 *wait_ms), $00000400
    long    MODIFY|FREQUENCY|(80 *wait_ms), $FFFFFC00
    long    JUMP,                           -3 *STEPS

SQW_BRASS1  'Squeaky Brass (could use some improvement!)
    long    SET|VOLUME,                     $BE980000
    long    JUMP,                           1 *STEPS
SQW_BRASS2
    long    SET|VOLUME,                     $A6980000
    long    SET|MODULATION,                 $00000000|$159
    long    SET|ENVELOPE|(5 *wait_ms),      $002703C0|$008
    long    SET|ENVELOPE,                   $FFFFCA00|$008
    long    MODIFY|FREQUENCY|(120 *wait_ms),$00000000
    long    MODIFY|FREQUENCY|(34 *wait_ms), $FFFFFD30
    long    SET|MODULATION,                 $00000800|$000
    long    MODIFY|FREQUENCY|(69 *wait_ms), $000002D0
    long    SET|ENVELOPE,                   $00003600|$008
    long    SET|MODULATION,                 {$FFFFF800|}$000
    long    MODIFY|FREQUENCY|(69 *wait_ms), $FFFFFD30
    long    JUMP,                           -6 *STEPS

SAW_HLIGHT_trg1 long    JUMP, @SAW_HLIGHT_trg1_code-@SAW_HLIGHT_trg1-STEPS
SAW_HLIGHT  'Clean sound with fade-out
    long    MODIFY|VOLUME,                  $FBBB0000
    long    SET|ENVELOPE|(20 *wait_ms),     $003A0200|$008
    long    SET|ENVELOPE,                   $FFF8FC00|$001
    long    JUMP,                           -1 *STEPS

SAW_HLIGHT_trg1_code
    long    SET|VOLUME|(1 *wait_ms),        $00000000 'TRG_1 = Reset volume
    long    SET|FREQUENCY,                  $00000000
    long    SET|ENVELOPE|(9 *wait_ms),      $00000000|$000
    long    SET|MODULATION,                 $00000000|$100
    long    SET|VOLUME,                     $FF001000
    long    JUMP,                           -1 *STEPS

SAW_BLEEP 'background bleeps
    long    SET|VOLUME,                     $00000000
    long    SET|ENVELOPE,                   $05A41A00|$108
    long    SET|MODULATION,                 $00000000|$043
    long    SET|VOLUME|(38 *wait_ms),       $FC5A0000
    long    SET|ENVELOPE,                   $FDADF300|$001
    long    JUMP,                           -1 *STEPS

SAW_ACCOMP 'Like SQW_VIBRA, but less vibrato and no PWM
    long    SET|MODULATION,                 $00000000|$100
    long    SET|VOLUME,                     $C0000000
    long    SET|ENVELOPE,                   $0010CC00|$098
    long    MODIFY|FREQUENCY|(5 *wait_ms),  $FFFFFE00
    long    WAIT|(5 *wait_ms),              0
    long    SET|MODULATION,                 $00000000|$0B0
    long    MODIFY|FREQUENCY|(10 *wait_ms), $FFFFFE00
    long    WAIT|(10 *wait_ms),             0
    long    SET|ENVELOPE,                   $FFFC5800|$000
    long    MODIFY|FREQUENCY|(5 *wait_ms),  $FFFFFE00
    long    WAIT|(45 *wait_ms),             0
    long    SET|ENVELOPE,                   $FFFEF200|$000
    long    MODIFY|FREQUENCY|(80 *wait_ms), $00000200
    long    SET|ENVELOPE,                   $FFFF5A00|$000
    long    MODIFY|FREQUENCY|(80 *wait_ms), $FFFFFE00
    long    SET|ENVELOPE,                   $FFFF7000|$000
    long    MODIFY|FREQUENCY|(80 *wait_ms), $00000200
    long    SET|ENVELOPE,                   $FFFF8800|$000
    long    MODIFY|FREQUENCY|(80 *wait_ms), $FFFFFE00
    long    SET|ENVELOPE,                   $FFFF9400|$001
    long    MODIFY|FREQUENCY|(80 *wait_ms), $00000200
    long    MODIFY|FREQUENCY|(80 *wait_ms), $FFFFFE00
    long    JUMP,                           -3 *STEPS


{   long    SET|MODULATION,                 $00000000|$042
    long    SET|VOLUME,                     $FF000000
    long    SET|ENVELOPE|(20 *wait_ms),     $002A0200|$008
    long    SET|ENVELOPE,                   $FFFDFC00|$001
    long    JUMP,                           -1 *STEPS}

SQW_BASS_trg1    long    JUMP, @SQW_BASS_bend1-@SQW_BASS_trg1-STEPS ''TRG_1: Pitchbend down
SQW_BASS 'Bass
    long    SET|MODULATION,                 $F3333000|$100
    long    SET|VOLUME,                     $7A000000
    long    SET|ENVELOPE,                   $00B80A00|$006
    long    WAIT|(6 *wait_ms),              0
    long    SET|ENVELOPE,                   $FFFF8000|$001
    long    JUMP,                           -1 *STEPS

SQW_BASS_bend1    ''Pitchbend down
    long    SET|ENVELOPE,                   $FFFFFF00|$001
    long    MODIFY|FREQUENCY|(175 *wait_ms),$FFFFF000
    long    JUMP,                           -1 *STEPS

'Triangly Percussion
TRI_PREC_trg3    long    JUMP,     @TRI_PREC_SNARE-@TRI_PREC_trg3-STEPS ''TRG_3 (low snare)
TRI_PREC_trg2    long    JUMP,     @TRI_PREC_CYMBAL-@TRI_PREC_trg2-STEPS ''TRG_2 (cymbal)
TRI_PERC_trg1    long    JUMP,     @TRI_PREC_SHAKE-@TRI_PERC_trg1-STEPS ''TRG_1 (Shaker)
TRI_PREC_trg0    ''TRG_0 (Kick)

                                                     
TRI_PREC_KICK
    ''Kick/Base
    long    SET|MODULATION,                 $FFFFFFFF
    long    SET|VOLUME,                     $FF000000
    long    SET|FREQUENCY,                  $F96F96F9
    long    SET|ENVELOPE,                   $00F80A00|$080
    long    MODIFY|FREQUENCY|(5 *wait_ms),  $B2CF9020
    long    SET|FREQUENCY,                  $009A02A0
    long    SET|VOLUME,                     $FF000000
    long    SET|ENVELOPE,                   $FFFCA200|$008
    long    MODIFY|FREQUENCY|(250 *wait_ms), $FFFFD900
    long    SET|VOLUME,                     $00000000
    long    JUMP,                           -1 *STEPS
TRI_PREC_SHAKE
    ''Shakery something i don't know
    long    SET|MODULATION,                 $FFFFFFFF
    long    SET|VOLUME,                     $EF000000
    long    SET|ENVELOPE,                   $00059800|$008
    long    SET|FREQUENCY,                  $04444444
    long    SET|VOLUME,                     $7F000000
    long    MODIFY|FREQUENCY|(90 *wait_ms), $B2CF9020
    long    SET|ENVELOPE,                   $FF57F400|$008
    long    MODIFY|FREQUENCY|(10 *wait_ms), $B2CF9020
    long    SET|VOLUME,                     $00000000
    long    JUMP,                           -1 *STEPS
TRI_PREC_CYMBAL  
    '' cymbal
    long    SET|MODULATION,                 $FFFFFFFF
    long    SET|VOLUME,                     $FF000000
    long    SET|FREQUENCY,                  $024C24C2
    long    SET|ENVELOPE,                   $00A80A00|$008
    long    MODIFY|FREQUENCY|(4 *wait_ms),  $B2CF9020
    long    SET|FREQUENCY,                  $04444444
    long    MODIFY|FREQUENCY|(2 *wait_ms),  $FFEB33C8
    long    MODIFY|FREQUENCY|(9 *wait_ms),  $EFFD6FC0
    long    MODIFY|FREQUENCY|(10 *wait_ms), $EFFFE4A8
    long    SET|VOLUME,                     $FF000000
    long    SET|ENVELOPE,                   $FFFE9800|$001
    long    MODIFY|FREQUENCY|(600 *wait_ms), $B2CF9020
    long    SET|VOLUME,                     $00000000
    long    JUMP,                           -1 *STEPS
TRI_PREC_SNARE
    '' snare
    long    SET|MODULATION,                 $FFFFFFFF
    long    SET|VOLUME,                     $FF000000
    long    SET|FREQUENCY,                  $024C24C2
    long    SET|ENVELOPE,                   $00A80A00|$008
    long    MODIFY|FREQUENCY|(4 *wait_ms),  $B2CF9020
    long    SET|FREQUENCY,                  $03483483
    long    MODIFY|FREQUENCY|(2 *wait_ms),  $FFEB33C8
    long    MODIFY|FREQUENCY|(9 *wait_ms),  $FFFD6FC0
    long    MODIFY|FREQUENCY|(10 *wait_ms), $FFFFE4A8
    long    SET|VOLUME,                     $8C000000
    long    SET|ENVELOPE,                   $FFFCA200|$008
    long    MODIFY|FREQUENCY|(250 *wait_ms), $B2CF9020
    long    SET|VOLUME,                     $00000000
    long    JUMP,                           -1 *STEPS

    

    
DAT

byte "-RETROSNDFOOT-"


                   
CON

  END         = $0
  OCTAVE      = 1<<12
  NOTE        = 1<<8
  INSTR       = 1<<3
  RESTART     = -1
  SET_TEMPO   = 32768
  ___         = 25 << 3      ' Void note
  TRG_0       = 12 << 3
  TRG_1       = 26 << 3
  TRG_2       = 27 << 3  
  TRG_3       = 28 << 3  
  TRG_4       = 29 << 3  
  TRG_5       = 30 << 3  
  TRG_6       = 31 << 3  
  ms          = 45_180_458'55_063_683
  Hz          = ms / 1000
  seconds     = Hz   
  repeats     = $10
  delta       = 12
  wait_ms     = repeats
  STEPS       = 8
  VOL         = 1 << 24
  BPM         = 11_811_160'9_691_208 ' (2^32 / 78_000 / 11) * 16
  
  ' Instructions
  JUMP        = MODIFY|PROGRAM_CNT
  SET         = %0000
  MODIFY      = %1000
  WAIT        = MODIFY
  
  ' Registers
  FREQUENCY   = $0
  PHASE       = $1
  MODULATION  = $2
  MODPHASE    = $3
  ENVELOPE    = $4
  VOLUME      = $5
  ENVELOPE_NOW= $6
  PROGRAM_CNT = $7
        

  ' Masks
  LSB9         = %00000000_00000000_00000001_11111111
  ILSB9        = %11111111_11111111_11111110_00000000
  