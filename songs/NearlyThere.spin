PUB melody
  return @MUSIC
  
OBJ
header : "retronitus_header"

DAT 
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                                     Music header
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
            byte "-RETROSNDTITLE-"
            byte "Nearly There",0
            byte "-RETROSNDCOMMENT-"
            byte "Based on 'A Prehistoric Tale (main theme)'",0
            byte "-RETROSNDAUTHOR-"
            byte "Johannes Ahlebrand",0
            byte "-RETROSNDCHANNELS-"
            byte long header#WAVE_SQUARE | header#PAN_CENTER | (header#AMPLIFY * 1) 
            byte long header#WAVE_SQUARE | header#PAN_CENTER | (header#AMPLIFY * 1) 
            byte long header#WAVE_SQUARE | header#PAN_CENTER | (header#AMPLIFY * 1) 
            byte long header#WAVE_SAWTOOTH | header#PAN_CENTER 
            byte long header#WAVE_SAWTOOTH | header#PAN_CENTER 
            byte long header#WAVE_SAWTOOTH | header#PAN_CENTER 
            byte long header#WAVE_TRIANGLE | header#PAN_CENTER  | (header#AMPLIFY * 1) 
            byte long header#WAVE_OLDNOISE | header#PAN_CENTER | (header#AMPLIFY * 1)

            'header#WAVE_NONE'
            long 1
                                        
            byte "-RETROSNDHEAD-" 
MUSIC         word  0
'──────────────────────────────────────────────────────────────────────────────────────────
B 
SEQ_INIT      word  @PATTSEQ_BASS                 -(@B-RLOC)   ' Square wave channel 1
              word  0                                          ' Square wave channel 2
              word  @PATTSEQ_LEAD                 -(@B-RLOC)   ' Square wave channel 3 
              word  @PATTSEQ_COORD_VOICE1_1       -(@B-RLOC)   ' Saw wave channel 2 
              word  @PATTSEQ_COORD_VOICE2_1       -(@B-RLOC)   ' Saw wave channel 3 
              word  @PATTSEQ_COORD_VOICE3_1       -(@B-RLOC)   ' Saw wave channel 1  
              word  @PATTSEQ_PERCUSSION2_TRIANGLE -(@B-RLOC)   ' Triangle channel
              word  @PATTSEQ_PERCUSSION1_NOISE    -(@B-RLOC)   ' Noise channel
'──────────────────────────────────────────────────────────────────────────────────────────
INS_INIT      word  @TRI_PERC      -(@B-RLOC)  
              word  @NOISE_PERC    -(@B-RLOC)
              word  @SQR_BEEP      -(@B-RLOC)
              word  @SAW_LEAD      -(@B-RLOC)
              word  @SAW_PIANO     -(@B-RLOC) 
              word  @SQR_STACC     -(@B-RLOC) 
              word  END     
                         
DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                               Pattern sequency data
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
  long 1

PATTSEQ_COORD_VOICE1_1
'{
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *6) | (NOTE *10)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *8)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8)               | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *7)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *6) | (NOTE *10)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *8)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8)               | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *7)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
'}
'{
  ' Part 2
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
'}
  ' Part 3
'{
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
'}
  ' Part 4
'{
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS        -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *7 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS        -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *10)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS        -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *7 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS        -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
'}
  ' Part 5                                                                                      
'{
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
'}
  word  13
  
PATTSEQ_COORD_VOICE2_1
'{
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *7) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *7) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *4)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
'}
'{   
  ' Part 2 
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE  -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE  -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE  -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE  -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE2 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE2 -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE2 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE2 -(@B-RLOC))<<1 
'}
'{
  ' Part 3
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1   
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1   
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
'}
'{
  ' Part 4
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4_SHORT     -(@B-RLOC))<<1   
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *4) |3 , (@PATT_LEAD4_SHORT_VAR -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4           -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4_SHORT     -(@B-RLOC))<<1   
  word (OCTAVE *6) | (NOTE *3)               | (INSTR *4) |3 , (@PATT_LEAD4_SHORT_VAR -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4           -(@B-RLOC))<<1 
'}
'{
  ' Part 5
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1   
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1   
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
'}
  word  13
  
PATTSEQ_COORD_VOICE3_1
'{
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *6) | (NOTE *7)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *10)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *6) | (NOTE *10)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *10)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *6) | (NOTE *7)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *10)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS      -(@B-RLOC))<<1    
  word (OCTAVE *6) | (NOTE *10)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_HALF -(@B-RLOC))<<1
'}
'{   
  ' Part 2  
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1 
'}
  ' Part 3
'{
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
'}
  ' Part 4
'{
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS        -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *10)              | (INSTR *4) |3 , (@PATT_COORD_TAPS        -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS        -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *10)              | (INSTR *4) |3 , (@PATT_COORD_TAPS        -(@B-RLOC))<<1
  word (OCTAVE *5) | (NOTE *5)               | (INSTR *4) |3 , (@PATT_COORD_TAPS        -(@B-RLOC))<<1  
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *4) |3 , (@PATT_COORD_TAPS        -(@B-RLOC))<<1 
'}
  ' Part 5
'{
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1 
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *12)              | (INSTR *4) |3 , (@PATT_COORD_TAPS_DOUBLE -(@B-RLOC))<<1
'}
  word  13
DAT
PATTSEQ_PERCUSSION1_NOISE
'{
  word  ((((BPM*105)*64)>>15)&$FFF0)|1          , (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1    
  word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1
'}
'{
  ' Part 2 
  word  ((((BPM*105)*64)>>15)&$FFF0)|1          , (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1    
  word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1
  word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1    
  word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1
'}
  ' Part 3
'{
  word  ((((BPM*105)*64)>>15)&$FFF0)|1          , (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1    
  word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1
  word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1    
  word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1
'}
  ' Part 4
'{
  word  ((((BPM*105)*64)>>15)&$FFF0)|1       ,    (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1    
  word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1
  word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1    
  word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1
'}
  ' Part 5
'{
  word  ((((BPM*105)*64)>>15)&$FFF0)|1          , (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1    
  word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1
  'word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1    
  'word                                         (INSTR *1) |3 , (@PATT_PERCUSSION1_NOISE    -(@B-RLOC))<<1
'}
  word  13
DAT
PATTSEQ_PERCUSSION2_TRIANGLE
'{
  word  {SET_TEMPO|(((BPM*105)/8)&$FFF00000)  |} (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1   
  'word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1  
'}
  ' Part 2
'{
  {word  {SET_TEMPO|(((BPM*105)/8)&$FFF00000)  |} (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1    
  word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1  
  'word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1    
  'word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1
'}
  ' Part 3
'{
  word  {SET_TEMPO|(((BPM*105)/8)&$FFF00000)  |} (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1    
  word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1  
  'word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1    
  'word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1
'}

  ' Part 4
'{
  word  {SET_TEMPO|(((BPM*105)/8)&$FFF00000)  |} (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1    
  word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1  
  'word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1    
  'word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1
'}
  ' Part 5
'{
  word  {SET_TEMPO|(((BPM*105)/8)&$FFF00000)  |} (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1   
  'word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1  
  'word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1    
  'word                                         (INSTR *0) |3 , (@PATT_PERCUSSION2_TRIANGLE -(@B-RLOC))<<1
'}
  word  13

DAT
PATTSEQ_LEAD
'{
  word (OCTAVE *6) | (NOTE *7)               | (INSTR *3) |3 , (@PATT_LEAD1 -(@B-RLOC))<<1    
  word (OCTAVE *6) | (NOTE *7)               | (INSTR *3) |3 , (@PATT_LEAD1 -(@B-RLOC))<<1  
  word (OCTAVE *6) | (NOTE *7)               | (INSTR *3) |3 , (@PATT_LEAD2 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *7)               | (INSTR *3) |3 , (@PATT_LEAD3 -(@B-RLOC))<<1  
'}
  ' Part 2
'{
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *5) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *5) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *5) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *5) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *5) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *5) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *5) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *5) |3 , (@PATT_LEAD4 -(@B-RLOC))<<1
'}
  ' Part 3
'{
  word (OCTAVE *5) | (NOTE *5)               | (INSTR *3) |3 , (@PATT_LEAD5 -(@B-RLOC))<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *3) |3 , (@PATT_LEAD6 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *3) |3 , (@PATT_LEAD7 -(@B-RLOC))<<1
  word (OCTAVE *5) | (NOTE *5)               | (INSTR *3) |3 , (@PATT_LEAD5 -(@B-RLOC))<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *3) |3 , (@PATT_LEAD8 -(@B-RLOC))<<1
  word (OCTAVE *5) | (NOTE *5)               | (INSTR *3) |3 , (@PATT_LEAD7 -(@B-RLOC))<<1
'}                                                                                  
  ' Part 4                                                                          
'{
  word (OCTAVE *6) | (NOTE *8)               | (INSTR *3) |3 , (@PATT_LEAD9 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *8)               | (INSTR *3) |3 , (@PATT_LEAD9 -(@B-RLOC))<<1
'}
  ' Part 5
'{
  word (OCTAVE *5) | (NOTE *5)               | (INSTR *3) |3 , (@PATT_LEAD5 -(@B-RLOC))<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *3) |3 , (@PATT_LEAD6 -(@B-RLOC))<<1
  word (OCTAVE *6) | (NOTE *5)               | (INSTR *3) |3 , (@PATT_LEAD7 -(@B-RLOC))<<1
  word (OCTAVE *5) | (NOTE *5)               | (INSTR *3) |3 , (@PATT_LEAD5 -(@B-RLOC))<<1
  word (OCTAVE *5) | (NOTE *0)               | (INSTR *3) |3 , (@PATT_LEAD8 -(@B-RLOC))<<1
  word (OCTAVE *5) | (NOTE *5)               | (INSTR *3) |3 , (@PATT_LEAD7 -(@B-RLOC))<<1
'}
  word 13

DAT
PATTSEQ_BASS
'{
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1    
  word (OCTAVE *9) | (NOTE *3)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1  
  word (OCTAVE *9) | (NOTE *1)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1
  word (OCTAVE *9) | (NOTE *0)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1  
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1    
  word (OCTAVE *9) | (NOTE *3)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1  
  word (OCTAVE *9) | (NOTE *1)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1
  word (OCTAVE *9) | (NOTE *0)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1
'}
  ' Part 2    
'{
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *1)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *0)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *1)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *0)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1
'}
  ' Part 3
'{   
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *1)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *0)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *1)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *0)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1
'}
  ' Part 4
'{   
  word (OCTAVE *9) | (NOTE *1)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *3)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *1)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *3)               | (INSTR *2) |3 , (@PATT_BASS -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1
'}
  ' Part 5
'{   
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *1)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *0)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *1)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *0)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1   
  word (OCTAVE *9) | (NOTE *5)               | (INSTR *2) |3 , (@PATT_BASS_LONG -(@B-RLOC))<<1
'}
  'long END
  word 13
word $fffe ' END of pattseqs

DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'                                  Pattern data
'──────────────────────────────────────────────────────────────────────────────────────────
PATT_LEAD1               byte ___             | 3
                         byte ((12    ) << 3) | 1
                         byte ((12 + 1) << 3) | 1
                         byte ((12 - 1) << 3) | 3
                         byte ((12 - 2) << 3) | 1
                         byte ((12 + 7) << 3) | 7
                         byte ___             | 7
                         byte ___             | 5
                         byte ((12 - 5) << 3) | 1
                         byte ((12 + 1) << 3) | 1
                         byte ((12 - 1) << 3) | 3
                         byte ((12 - 2) << 3) | 1
                         byte ((12 + 7) << 3) | 5
                         byte ((12 - 2) << 3) | 3
                         byte ((12 - 2) << 3) | 3
                         byte ((12 - 1) << 3) | 3
                         byte END

PATT_LEAD2               byte ___             | 3
                         byte ((12    ) << 3) | 1
                         byte ((12 + 1) << 3) | 1
                         byte ((12 - 1) << 3) | 3
                         byte ((12 - 2) << 3) | 1
                         byte ((12 + 12)<< 3) | 7
                         byte ___             | 7
                         byte ___             | 5
                         byte ((12 - 10)<< 3) | 1
                         byte ((12 + 1) << 3) | 1
                         byte ((12 - 1) << 3) | 3
                         byte ((12 - 2) << 3) | 1
                         byte ((12 + 12)<< 3) | 5
                         byte ((12 + 2) << 3) | 3
                         byte ((12 - 4) << 3) | 7
                         byte END

PATT_LEAD3               byte ___             | 3
                         byte ((12    ) << 3) | 1
                         byte ((12 + 1) << 3) | 1
                         byte ((12 - 1) << 3) | 3
                         byte ((12 - 2) << 3) | 1
                         byte ((12 + 12)<< 3) | 3
                         byte ((12 + 2) << 3) | 3
                         byte ((12 - 2) << 3) | 5
                         byte ((12 - 1) << 3) | 3
                         byte ((12 + 1) << 3) | 5
                         byte ((12 + 0) << 3) | 3
                         byte ((12 + 0) << 3) | 5
                         byte ((12 - 1) << 3) | 5
                         byte ((12 - 4) << 3) | 5
                         byte ((12 - 2) << 3) | 3        
                         byte END

PATT_LEAD4               byte ___             | 5
                         byte ((12    ) << 3) | 5 
                         byte ((12 - 5) << 3) | 3
                         byte ((12 - 2) << 3) | 3
                         byte ((12 + 9) << 3) | 5  
                         byte ((12 + 1) << 3) | 3  
                         byte ((12 - 3) << 3) | 1
PATT_LEAD4_SHORT         byte ___             | 5
                         byte ((12    ) << 3) | 5 
                         byte ((12 - 5) << 3) | 3
                         byte ((12 - 2) << 3) | 3
                         byte ((12 + 9) << 3) | 5  
                         byte ((12 + 1) << 3) | 3  
                         byte ((12 - 3) << 3) | 1
                         byte END

PATT_LEAD4_SHORT_VAR     byte ___             | 5
                         byte ((12    ) << 3) | 5 
                         byte ((12 - 5) << 3) | 3
                         byte ((12 - 2) << 3) | 3
                         byte ((12 + 9) << 3) | 5  
                         byte ((12 + 3) << 3) | 3  
                         byte ((12 - 3) << 3) | 1
                         byte END

PATT_LEAD5               byte ((12    ) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ((12 + 3) << 3) | 5
                         byte ((12 + 2) << 3) | 5
                         byte ((12 - 2) << 3) | 3  
                         byte ((12 - 1) << 3) | 5  
                         byte ((12 - 2) << 3) | 5
                         byte ((12 - 2) << 3) | 3 
                         byte ((12 + 2) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ((12 - 4) << 3) | 0
                         byte ((12 - 1) << 3) | 1  
                         byte ((12 - 2) << 3) | 0  
                         byte ((12 - 2) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 3 
                         byte END

PATT_LEAD6               byte ((12    ) << 3) | 5
                         byte ((12    ) << 3) | 5
                         byte ((12    ) << 3) | 3
                         byte ((12 - 2) << 3) | 5 
                         byte ((12 - 2) << 3) | 5
                         byte ((12 - 1) << 3) | 3
                         byte ((12 + 1) << 3) | 5  
                         byte ((12    ) << 3) | 5
                         byte ((12    ) << 3) | 3
                         byte ((12 - 1) << 3) | 5 
                         byte ((12 - 4) << 3) | 5
                         byte ((12 - 3) << 3) | 3
                         byte END

PATT_LEAD7               byte ((12    ) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7 
                         byte ___             | 7
                         byte ___             | 7
                         byte ((12    ) << 3) | 7
                         byte ___             | 7 
                         byte END

PATT_LEAD8               byte ((12    ) << 3) | 5
                         byte ((12    ) << 3) | 5
                         byte ((12    ) << 3) | 3
                         byte ((12 - 2) << 3) | 5 
                         byte ((12 - 2) << 3) | 5
                         byte ((12 - 1) << 3) | 3
                         byte ((12 + 1) << 3) | 5  
                         byte ((12 + 2) << 3) | 5
                         byte ((12 + 2) << 3) | 3
                         byte ((12 + 3) << 3) | 5 
                         byte ((12 + 5) << 3) | 5
                         byte ((12 - 5) << 3) | 3
                         byte END

PATT_LEAD9               byte ((12    ) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ((12 + 2) << 3) | 5
                         byte ((12 - 3) << 3) | 5 
                         byte ((12 - 4) << 3) | 3
                         byte ((12 - 2) << 3) | 5
                         byte ((12 + 2) << 3) | 5  
                         byte ((12 + 4) << 3) | 3
                         byte ((12 + 1) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ((12 - 3) << 3) | 7 
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte END
   
PATT_COORD_TAPS_DOUBLE2  byte ((12 + 0 ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 10) << 3) | 1
                         byte ((12 + 2 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 10) << 3) | 1
                         byte ((12 + 2 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 10) << 3) | 1
                         byte ((12 + 2 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 10) << 3) | 1
                         byte ((12 + 2 ) << 3) | 1
                         byte END
  
PATT_COORD_TAPS_DOUBLE   byte ((12 + 0 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 5
                         byte ((12 + 0 ) << 3) | 3
                         byte ((12 + 0 ) << 3) | 3
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 3
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1 
                         byte ((12 + 12) << 3) | 3

PATT_COORD_TAPS          byte ((12 + 0 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 5
                         byte ((12 + 0 ) << 3) | 3
                         byte ((12 + 0 ) << 3) | 3
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 3
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1 
                         byte ((12 + 12) << 3) | 3
                         byte END
                         
PATT_COORD_TAPS_HALF     byte ((12 + 0 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 5
                         byte ((12 + 0 ) << 3) | 3
                         byte ((12 + 0 ) << 3) | 1
                         byte END

PATT_BASS_LONG           byte ((12     ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1 
                         byte ((12 - 12) << 3) | 1
                         byte ((12     ) << 3) | 3 
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12     ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         
PATT_BASS                byte ((12     ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1 
                         byte ((12 - 12) << 3) | 1
                         byte ((12     ) << 3) | 3 
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12     ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1                           
                         byte END
                         
PATT_PERCUSSION1_NOISE   byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 0
                         byte TRG_0 | 0
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 0
                         byte TRG_0 | 0
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 0
                         byte TRG_0 | 0
                         byte TRG_0 | 0
                         byte TRG_0 | 0
                         byte END

PATT_PERCUSSION2_TRIANGLE
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3

                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3
                         
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3

                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_1 | 1
                         byte TRG_0 | 1
                         byte END

DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                                    Instrument data
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────          
         long 1
NOISE_PERC
         long  SET|MODULATION                   , 2_895_977_631          
         long  SET|VOLUME                       , 165 *vol 
         long  SET|FREQUENCY                    , 49000 *Hz              
         long  SET|ENVELOPE| (5 *wait_ms)       ,( (ms/5) &ILSB9) | 7
         long  SET|ENVELOPE| (20 *wait_ms)      ,(-(ms/56) &ILSB9)| 1
         long  SET|MODULATION                   , 2_895_977_632 
         long  SET|ENVELOPE| (20 *wait_ms)      ,(-(ms/220) &ILSB9)| 1
         long  JUMP                             , -1 *STEPS              
'────────────────────────────────────────────────────────────────────────────────────────── 

TRI_PERC_trg1    long    JUMP,     @TRI_SNR-@TRI_PERC_trg1 -STEPS ''TRG_1
TRI_PERC long  SET|MODULATION                   , 27 << 27          
         long  SET|VOLUME                       , 255 *vol 
         long  SET|FREQUENCY                    , 700 *Hz              
         long  SET|ENVELOPE                     ,( (ms/5) &ILSB9) | 7
         long  MODIFY|FREQUENCY | (repeats *6)  , -70_20_00
         long  SET|MODULATION                   , -1
         long  MODIFY|FREQUENCY | (repeats *30) , -2_43_75 
         long  MODIFY|FREQUENCY | (repeats *100), -0_48_75 
         long  SET|ENVELOPE                     ,(-(ms/38) &ILSB9) | 1
         long  JUMP                             , -1 *STEPS

TRI_SNR
    '' snare
    long    SET|VOLUME|(1 *wait_ms),        $00000000   
    long    SET|PHASE,                      $70000000
    long    SET|MODULATION,                 $CCCCCCCC
    long    SET|VOLUME,                     $FF000000
    long    SET|FREQUENCY,                  $024C22E4'$04444444'$01F81DE8'600*Hz
    long    SET|ENVELOPE,                   $00F80A00|$008   
    long    MODIFY|FREQUENCY|(3 *wait_ms),  $B2CF9020
    long    SET|FREQUENCY,                  $025A31D8'1000 *Hz
    long    MODIFY|FREQUENCY|(1 *wait_ms),  -1_363_000'$FFEB33C8 '- ($035A31D8/(3*8))
    long    MODIFY|FREQUENCY|(5 *wait_ms),  -168_000
    long    MODIFY|FREQUENCY|(7 *wait_ms),  -7_000         
    long    SET|VOLUME,                     $C0000000
    long    SET|ENVELOPE,                   (-(ms/150) &ILSB9) | 1           
    long    MODIFY|FREQUENCY|(30 *wait_ms), $83CF9020
    long    MODIFY|FREQUENCY|(170 *wait_ms), $B2CF9020
    long    SET|VOLUME,                     $00000000
    long    JUMP,                           -1 *STEPS  
{                       
TRI_SNR  long  SET|MODULATION                   , -1          
         long  SET|VOLUME                       , 255 *vol 
         long  SET|FREQUENCY                    , 700 *Hz              
         long  SET|ENVELOPE                     ,( (ms/5) &ILSB9) | 7
         long  MODIFY|FREQUENCY | (repeats *4)  , -1_295_020_000
         
         long  SET|FREQUENCY                    , 1000 *Hz 
         long  MODIFY|FREQUENCY | (repeats *2)  , -1_363_000
         long  MODIFY|FREQUENCY | (repeats *9)  , -168_000 
         long  MODIFY|FREQUENCY | (repeats *10) , -7_000
         long  SET|VOLUME                       , 140 *vol 
         long  SET|ENVELOPE                     ,(-(ms/308) &ILSB9) | 1  
         long  MODIFY|FREQUENCY | (repeats *250), -1_295_020_000  
         long  JUMP                             , -1 *STEPS
}           
'────────────────────────────────────────────────────────────────────────────────────────── 
SQR_BEEP long  SET|MODULATION                   , 220                    
         long  SET|MODULATION                   , 45 << 9
         long  SET|VOLUME                       , 100 *vol                
         long  SET|ENVELOPE|(20 *wait_ms)       ,( (ms/20 )&ILSB9) | 7    
         long  SET|ENVELOPE|(8  *wait_ms)       ,(-(ms/190)&ILSB9) | 1  
         long  SET|ENVELOPE                     ,(-(ms/420)&ILSB9) | 1  
         long  JUMP                             , -1 *STEPS              
'────────────────────────────────────────────────────────────────────────────────────────── 
SAW_LEAD long  SET|VOLUME                       , 36 *vol                
         long  SET|ENVELOPE                     ,( (ms/6000) &ILSB9)| 7    
         long  SET|MODULATION                   , 58                    
         long  SET|MODULATION|(200 *wait_ms)    , 15  << 9
         long  SET|MODULATION|(300 *wait_ms)    , 205 << 9 
         long  SET|ENVELOPE  |(120 *wait_ms)    ,(-(ms/4900)&ILSB9)| 1  
         long  SET|ENVELOPE                     ,(-(ms/29000)&ILSB9)| 1
         long  MODIFY|FREQUENCY|(repeats *80)   , -1500
         long  MODIFY|FREQUENCY|(repeats *160)  ,  1500
         long  MODIFY|FREQUENCY|(repeats *160)  , -1500
         long  JUMP                             , -3 *STEPS              
'────────────────────────────────────────────────────────────────────────────────────────── 
SAW_PIANO
         long  SET|MODULATION                   , 218                    
         long  SET|MODULATION                   , 36 << 9
         long  SET|VOLUME                       , 77 *vol                
         long  SET|ENVELOPE|(20 *wait_ms)       ,( (ms/20 )&ILSB9) | 7    
         long  SET|ENVELOPE|(15 *wait_ms)       ,(-(ms/600)&ILSB9) | 1  
         long  SET|MODULATION                   , 100 << 9 
         long  SET|ENVELOPE|(100 *wait_ms)      ,(-(ms/700)&ILSB9) | 1  
         long  SET|MODULATION                   , 220 << 9 
         long  SET|ENVELOPE                     , 0 | 1 
         long  JUMP                             , -1 *STEPS

SQR_STACC
         long  SET|MODULATION                   , 133                    
         long  SET|MODULATION|(0 *wait_ms)      , 60 << 9 
         long  SET|VOLUME                       , 50 *vol                
         long  SET|ENVELOPE|(20 *wait_ms)       ,( (ms/5 )&ILSB9) | 7    
         long  SET|ENVELOPE|(8  *wait_ms)       ,(-(ms/150)&ILSB9) | 1
         long  SET|ENVELOPE|(20 *wait_ms)       ,(-(ms/2320)&ILSB9)| 1  
         long  SET|MODULATION|(0 *wait_ms)      , 305 << 9 
         long  JUMP                             , -1 *STEPS

DAT
byte "-RETROSNDFOOT-"
                   
CON
    END         = $0
    OCTAVE      = 1<<12
    NOTE        = 1<<8
    INSTR       = 1<<3
    SET_TEMPO   = 32768
    ___         = 25 << 3      ' Void note
    TRG_0       = 12 << 3
    TRG_1       = 26 << 3
    TRG_2       = 27 << 3  
    TRG_3       = 28 << 3  
    TRG_4       = 29 << 3  
    TRG_5       = 30 << 3  
    TRG_6       = 31 << 3  
    ms          = 67108864
    Hz          = ms / 1000
    seconds     = Hz   
    repeats     = $10 ' Not actually a single repeat, but 8(?) repeats
    delta       = 12
    wait_ms     = repeats ' Not actually a millisecond
    STEPS       = 8
    VOL         = 1 << 24
    BPM         = 366_048 / 16 ' (2^32 / 64_000 / 11) * 16

    DUMMY_NOTESET = (OCTAVE *6) | (NOTE *9) ' A4
    
    ' Instructions
    JUMP        = MODIFY|PROGRAM_CNT
    SET         = %0000
    MODIFY      = %1000
    WAIT        = MODIFY
    
    ' Registers
    FREQUENCY   = $0
    PHASE       = $1
    MODULATION  = $2
    MODPHASE    = $3
    ENVELOPE    = $4
    VOLUME      = $5
    ENVELOPE_NOW= $6
    PROGRAM_CNT = $7
        

    ' Masks
    LSB9         = %00000000_00000000_00000001_11111111
    ILSB9        = %11111111_11111111_11111110_00000000

    ' Gunk
    TODO_INSTR1 = JUMP
    TODO_INSTR2 = -1 * STEPS
    TODO = ___
    TODO_CHANNEL = 0

    RLOC = 0 ' TODO: relocate
  