CON
    _clkmode  = xtal1 | pll16x
    _xinfreq  = 5_000_000
    left_pin  = 11
    right_pin = 10

OBJ
    sound : "RedSand_retronitus"
    music : "RedSand"
    pst   : "parallax serial terminal"
    
PUB Main : count | pattcount,b,p,p2,chans,i,w
    pst.start(115200)
    sound.start(right_pin,left_pin)
    sound.playMusic(music.melody)
    repeat
       pst.str(string("Step count:",13))
       chans := music.melody + 2
       repeat i from 1 to 8
         count~
         if p := word[chans][i-1]                 
          p := word[chans][i-1] + music.melody + 2
          repeat until ((w:=word[p])&7)==%101
            p+=2
            ifnot (w&1)
              p2 := w>>1
              pattcount~
              pst.str(string("Pattern of Channel "))
              pst.dec(i)
              pst.str(string(" at $"))
              pst.hex(p2,4)
              pst.str(string(" has "))
              repeat while b:=byte[p2++]
                pattcount += (b&7)+1
              pst.dec(pattcount)
              pst.str(string(" steps. Total: "))
              count += pattcount
              pst.dec(count)
              pst.char(13)
              
          pst.str(string("Channel "))
          pst.dec(i)
          pst.str(string(" has "))
          pst.dec(count)
          pst.str(string(" steps!",13))
         else
          pst.str(string("Channel "))
          pst.dec(i)     
          pst.str(string(" is unused!",13)) 
       waitcnt(cnt + 80_000_000)
 